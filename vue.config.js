// const fs = require('fs');

module.exports = {
  // devServer: {
  //   https: {
  //     key: fs.readFileSync('./certs/server.key'),
  //     cert: fs.readFileSync('./certs/server.crt'),
  //   },
  // },

  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "./src/assets/styles/abstract/_variables.scss";
          @import "./src/assets/styles/abstract/_utilities.scss";
          @import "./src/assets/styles/mixins/_mixins_app.scss";
          @import "./src/assets/styles/mixins/_mixins_page_layout.scss";
        `,
      },
    },
  },
  chainWebpack: (config) => {
    /** **************************************************************** */
    /* Needed for proper compilation of sass global files for unit-test. */
    /** **************************************************************** */
    if (process.env.NODE_ENV === 'test') {
      const scssRule = config.module.rule('scss');
      scssRule.uses.clear();
      scssRule.use('null-loader').loader('null-loader');
    }

    /** ****************** */
    /* SVG loader config */
    /** ****************** */
    {
      const svgRule = config.module.rule('svg');
      svgRule.uses.clear();
      svgRule
        .oneOf('inline')
        .resourceQuery(/inline/)
        .use('vue-svg-loader')
        .loader('vue-svg-loader')
        .end()
        .end()
        .oneOf('external')
        .use('file-loader')
        .loader('file-loader')
        .options({
          name: 'assets/[name].[hash:8].[ext]',
        });
    }
  },
};
