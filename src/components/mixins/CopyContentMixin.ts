/**
 * Mixin to copy text data to device's clipboard.
 * A success feedback message is shown to the user after coping.
 */

import { Copier } from '@/types';
import { Component, Vue } from 'vue-property-decorator';

@Component
export default class CopyContentMixin extends Vue implements Copier {
  copyToClipboard(data: { name: string; value: string }) {
    const el = document.createElement('input');
    el.value = data.value;
    document.body.appendChild(el);
    el.select();
    el.setSelectionRange(0, 99999); // For mobile devices
    document.execCommand('copy');
    document.body.removeChild(el);
    (this as any).$showFeedback({
      type: 'success',
      title: `${data.name} copied`,
      description: data.value,
    });
  }
}
