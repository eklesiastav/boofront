/**
 * Manages browser events for:
 *
 * Resizing the h2 headline text in the page's headline section as the user
 * scrolls up and down. The resizeable text must be query-selectable using
 * (".heading h1");
 *
 * Preventing browser unload if they are unsaved changes.
 */
import { Component, Vue } from 'vue-property-decorator';

@Component
export default class PageEventsMixin extends Vue {
  mounted() {
    document.addEventListener('scroll', this.resizeHeading);
    // beforeunload seems to only work with events attached directly.
    window.onbeforeunload = this.preventUnload;
  }

  destroyed() {
    document.removeEventListener('scroll', this.resizeHeading);
    document.removeEventListener('beforeunload', this.preventUnload);
  }

  /**
   * Prevent browser unload if the user still has unsaved changes.
   */
  preventUnload() {
    return (this as any).isSaveRequired ? '' : null;
  }

  /**
   * Resize the page header as the user scrolls more than 50px from top.
   */
  resizeHeading() {
    /* eslint no-param-reassign: 0 */
    const heading = document.querySelector('.heading h1') as HTMLHeadingElement;
    const headingButtonSection = document.querySelector('.heading-toolbar-wrapper') as HTMLDivElement;
    const toolbar = document.querySelector('.heading-toolbar-wrapper .toolbar') as HTMLDivElement;
    if (
      document.body.scrollTop > 50
      || document.documentElement.scrollTop > 50
    ) {
      // Resize H1 headings
      if (heading) heading.style.fontSize = '1rem';
      // Resize header toolbar
      if (toolbar) toolbar.style.transform = 'scale(0.9)';
      if (headingButtonSection) headingButtonSection.style.paddingBottom = '12px';
    } else {
      // Reset H1 headings
      if (heading) heading.style.fontSize = 'calc(1.1rem + (24 - 18) * (100vw - 400px) / (1300 - 400))';
      // Reset header toolbar
      if (toolbar) toolbar.style.transform = 'scale(1)';
      if (headingButtonSection) headingButtonSection.style.paddingBottom = '16px';
    }
  }
}
