/**
 * Mixin to support saving or discarding changes when navigating away.
 * Any component using his mixin must provide implementation for detecting if
 * there are unsaved changes, property: 'isSaveRequired' and methods 'saveChanges'
 * and 'discardChanges'
 */

import { Component, Vue } from 'vue-property-decorator';
import { ValueError } from '@/exceptions';
import { NavigationGuardNext, Route } from 'vue-router';

Component.registerHooks(['beforeRouteLeave']);

@Component
export default class SaveGuardMixin extends Vue {
  mounted() {
    if (
      !('isSaveRequired' in this)
      || !('saveChanges' in this)
      || !('discardChanges' in this)
    ) {
      throw new ValueError(`Component must implement properties: 'isSaveRequired',
      'saveChanges' and 'discardChanges' to use 'saveGuardMixin'.`);
    }
  }

  isSaveModalVisible = false;

  isCancelModalVisible = false;

  discardThenNavigate() {
    (this as any).discardChanges();
    this.isSaveModalVisible = false;
    /* eslint @typescript-eslint/ban-ts-ignore:0 */
    // @ts-ignore
    const nextRoute = this.$router.history.pending.name;
    this.$router.push({ name: nextRoute });
  }

  async saveThenNavigate() {
    await (this as any).saveChanges();
    this.isSaveModalVisible = false;
    if (!(this as any).isSaveRequired) {
      /* eslint @typescript-eslint/ban-ts-ignore:0 */
      // @ts-ignore
      const nextRoute = this.$router.history.pending.name;
      this.$router.push({ name: nextRoute });
    }
  }

  beforeRouteLeave(to: Route, from: Route, next: NavigationGuardNext) {
    if ((this as any).isSaveRequired) {
      this.isSaveModalVisible = true;
    } else {
      next();
    }
  }
}
