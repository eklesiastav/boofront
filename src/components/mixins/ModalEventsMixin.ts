/**
 * Adds event listener to close modal windows using the escape key.
 * The page scrolling is also disabled when a modal window is visible.
 */

import { Component, Vue } from 'vue-property-decorator';

@Component
export default class ModalEventsMixin extends Vue {
  mounted() {
    document.addEventListener('keyup', this.escapeCloseModal);
    document.body.style.overflow = 'hidden';
  }

  destroyed() {
    document.removeEventListener('keyup', this.escapeCloseModal);
    document.body.style.overflow = 'visible';
  }

  escapeCloseModal(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.$emit('close');

      // Compatibility with old browsers (IE) that don't support event.key.
    } else if (event.keyCode === 27) {
      this.$emit('close');
    }
  }
}
