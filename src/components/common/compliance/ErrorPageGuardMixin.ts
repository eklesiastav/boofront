import { Component, Vue } from 'vue-property-decorator';
import { NavigationGuardNext, Route } from 'vue-router';

Component.registerHooks(['beforeRouteEnter']);

@Component
export default class ErrorPageGuardMixin extends Vue {
  beforeRouteEnter(to: Route, from: Route, next: NavigationGuardNext) {
    if (!from.name) next({ name: 'Home' });
    next();
  }
}
