import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';
import { VueConstructor } from 'vue';

/**
 * Register all base components (all components with 'Base' prefix in the
 * component directory) as global components.
 * @param Vue
 */
function registerGlobalComponents(Vue: VueConstructor) {
  const requireComponent = require.context('.', true, /Base[A-Z]\w+\.(vue|js)$/);
  requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);

    const componentName = upperFirst(
      camelCase(
        fileName
          .split('/')
          .pop()
          ?.replace(/\.\w+$/, ''),
      ),
    );

    Vue.component(componentName, componentConfig.default || componentConfig);
  });
}

export default registerGlobalComponents;
