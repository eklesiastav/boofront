import { RouteConfig } from 'vue-router';

const SubscriptionsDashboard = () => import(/* webpackChunkName: "subscriptionsDashboard" */ '../components/emailSubscriptions/views/SubscriptionsDashboard.vue');
const SubscriptionDetail = () => import(/* webpackChunkName: "subscriptionDetail" */ '../components/emailSubscriptions/views/SubscriptionDetail.vue');

const subscriptionsRoute: Array<RouteConfig> = [
  {
    path: 'email-subscriptions',
    name: 'SubscriptionsDashboard',
    component: SubscriptionsDashboard,
    meta: {
      title: 'Portal - My email subscriptions',
      metaTags: [],
    },
  },
  {
    path: 'email-subscriptions',
    name: 'SubscriptionDetail/:subscriptionId',
    component: SubscriptionDetail,
    meta: {
      metaTags: [],
    },
  },
];

export default subscriptionsRoute;
