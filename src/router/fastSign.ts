import { RouteConfig } from 'vue-router';

const FastSignDashboard = () => import(/* webpackChunkName: "fastSignDashboard" */ '../components/fastSign/views/FastSignDashboard.vue');
const FastSignDetail = () => import(/* webpackChunkName: "fastSignDetail" */ '../components/fastSign/views/FastSignDetail.vue');

const fastSignRoute: Array<RouteConfig> = [
  {
    path: 'fast-sign',
    name: 'FastSignDashboard',
    component: FastSignDashboard,
    meta: {
      title: 'Portal - My signed documents',
      metaTags: [],
    },
  },
  {
    path: 'fast-sign/:documentId',
    name: 'FastSignDetail',
    component: FastSignDetail,
    meta: {
      metaTags: [],
    },
  },
];

export default fastSignRoute;
