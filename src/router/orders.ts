import { RouteConfig } from 'vue-router';

const OrdersDashboard = () => import(/* webpackChunkName: "ordersDashboard" */ '../components/orders/views/OrdersDashboard.vue');
const orderDetail = () => import(/* webpackChunkName: "orderDetail" */ '../components/orders/views/OrderDetail.vue');

const ordersRoutes: Array<RouteConfig> = [
  {
    path: 'orders',
    name: 'OrdersDashboard',
    component: OrdersDashboard,
    meta: {
      title: 'Portal - My orders',
      metaTags: [],
    },
  },
  {
    path: 'orders/:orderId',
    name: 'orderDetail',
    component: orderDetail,
    meta: {
      metaTags: [],
    },
  },
];

export default ordersRoutes;
