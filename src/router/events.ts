import { RouteConfig } from 'vue-router';

const EventsDashboard = () => import(/* webpackChunkName: "eventsDashboard" */ '../components/schedulesAndEvents/views/EventsDashboard.vue');
const EventDetail = () => import(/* webpackChunkName: "eventDetail" */ '../components/schedulesAndEvents/views/EventDetail.vue');

const eventsRoute: Array<RouteConfig> = [
  {
    path: 'events',
    name: 'EventsDashboard',
    component: EventsDashboard,
    meta: {
      title: 'Portal - My schedules & events',
      metaTags: [],
    },
  },
  {
    path: 'events/:eventId',
    name: 'EventDetail',
    component: EventDetail,
    meta: {
      metaTags: [],
    },
  },
];

export default eventsRoute;
