import Vue from 'vue';
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from 'vue-router';
import BeforeEnterRouteGuard from '@/router/BeforeEnterRouteGuard';
import ordersRoutes from '@/router/orders';
import subscriptionsRoutes from '@/router/emailSubscriptions';
import eventsRoutes from '@/router/events';
import fastSignRoutes from '@/router/fastSign';

const GenericCenteredWithSidebar = () => import(/* webpackChunkName: "genericCenteredWithSidebar" */ '../layouts/GenericCenteredWithSidebar.vue');
const GenericCentered = () => import(/* webpackChunkName: "genericCentered" */ '../layouts/GenericCentered.vue');
const Sidebar = () => import(/* webpackChunkName: "sidebar" */ '../components/common/composition/Sidebar.vue');
const Settings = () => import(/* webpackChunkName: "settings" */ '../components/settings/views/Settings.vue');

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '',
    name: '',
    redirect: { name: 'OrdersDashboard' },
  },
  {
    path: '/u',
    name: 'Home',
    components: {
      sidebar: Sidebar,
      main: GenericCenteredWithSidebar,
    },
    redirect: { name: 'OrdersDashboard' },
    children: [
      ...ordersRoutes,
      ...subscriptionsRoutes,
      ...eventsRoutes,
      ...fastSignRoutes,
    ],
  },
  {
    path: '/settings',
    name: '',
    components: {
      main: GenericCentered,
    },
    redirect: { name: 'Settings' },
    children: [
      {
        path: '',
        name: 'Settings',
        component: Settings,
        meta: {
          title: 'Portal - Settings',
          metaTags: [],
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active-link',
  linkExactActiveClass: 'active-exact-link',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to: Route, from: Route, next: NavigationGuardNext) => {
  const beforeEnterRouteGuard = new BeforeEnterRouteGuard(to, from, next);
  beforeEnterRouteGuard.setPageTitleAndMeta();
  next();
});

export default router;
