import { Route, NavigationGuardNext } from 'vue-router';

class BeforeEnterRouteGuard {
  /* eslint no-useless-constructor: 0 */
  constructor(
    private readonly to: Route,
    private readonly from: Route,
    private readonly next: NavigationGuardNext,
  ) {}

  /**
   * Set custom page title and meta tags.
   * Ref: https://alligator.io/vuejs/vue-router-modify-head/
   */
  setPageTitleAndMeta() {
    const nearestWithTitle = this.to.matched
      .slice()
      .reverse()
      .find((r) => r.meta && r.meta.title);

    const nearestWithMeta = this.to.matched
      .slice()
      .reverse()
      .find((r) => r.meta && r.meta.metaTags);

    if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

    Array.from(
      document.querySelectorAll('[data-vue-router-controlled]'),
    ).map((el) => (el.parentNode as Node).removeChild(el));

    if (!nearestWithMeta) return;

    nearestWithMeta.meta.metaTags
      .map((tagDef: { [index: string]: string }) => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach((key) => {
          tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
      })
      .forEach((tag: HTMLMetaElement) => document.head.appendChild(tag));
  }
}

export default BeforeEnterRouteGuard;
