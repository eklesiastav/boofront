import { ValueError } from '@/exceptions';
import { LanguageCode } from '@/i18n/interfaceLanguages';
import i18n from './setup';

const en = import(/* webpackChunkName: "en" */ './languagePacks/en');
const es = import(/* webpackChunkName: "es" */ './languagePacks/es');
const fr = import(/* webpackChunkName: "fr" */ './languagePacks/fr');
const pt = import(/* webpackChunkName: "pt" */ './languagePacks/pt');
const zhHans = import(/* webpackChunkName: "zhHans" */ './languagePacks/zh-Hans');
const zhHant = import(/* webpackChunkName: "zhHant" */ './languagePacks/zh-Hant');

const getLanguageObject = (langString: LanguageCode) => {
  let langObject;
  switch (langString) {
    case LanguageCode.en:
      langObject = en;
      break;
    case LanguageCode.es:
      langObject = es;
      break;
    case LanguageCode.fr:
      langObject = fr;
      break;
    case LanguageCode.pt:
      langObject = pt;
      break;
    case LanguageCode['zh-Hans']:
      langObject = zhHans;
      break;
    case LanguageCode['zh-Hant']:
      langObject = zhHant;
      break;
    default:
      throw new ValueError('Unsupported language.');
  }
  return langObject;
};

const getCurrentLocale = () => i18n.locale;

const getCompiledLanguagePack = (lang: LanguageCode) => {
  const language = getLanguageObject(lang);
  /* eslint @typescript-eslint/ban-ts-ignore:0 */
  // @ts-ignore
  return language.then((messages: any) => {
    if (messages.default.en) return JSON.stringify(messages.default.en);
    return JSON.stringify(messages.default);
  });
};

const getAllCompiledLanguagePacks = () => {
  const allLangs: any[] = [];
  /* eslint no-restricted-syntax: 0 */
  for (const lang of ['en', 'es', 'fr', 'pt', 'zh-Hans', 'zh-Hant']) {
    /* eslint @typescript-eslint/ban-ts-ignore:0 */
    // @ts-ignore
    getCompiledLanguagePack(LanguageCode[lang])
      .then((messages: string) => { allLangs.push({ [lang]: messages }); });
  }
  return allLangs;
};

export {
  getCurrentLocale,
  getCompiledLanguagePack,
  getAllCompiledLanguagePacks,
};
