import Vue from 'vue';
import VueI18n from 'vue-i18n';
import { LanguageCode } from '@/i18n/interfaceLanguages';
import messages from './languagePacks/en';
import APIClient from '../services/APIClient';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages,
});

const loadedLanguages = ['en'];

function setI18nLanguage(lang: LanguageCode) {
  i18n.locale = lang;
  APIClient.setLanguageHeader(lang);
  /* eslint no-unused-expressions: */
  document.querySelector('html')?.setAttribute('lang', lang);
  return lang;
}

function loadLanguageAsync(lang: LanguageCode) {
  // If language is the same with default.
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang));
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang));
  }

  // If the language hasn't been loaded yet
  return import(/* webpackChunkName: "lang-[request]" */ `@/i18n/languagePacks/${lang}`).then(
    /* eslint no-shadow: 0 */
    (messages) => {
      i18n.setLocaleMessage(lang, messages.default);
      loadedLanguages.push(lang);
      return setI18nLanguage(lang);
    },
  );
}

export { i18n as default, loadLanguageAsync };
