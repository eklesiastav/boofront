import orders from './orders';

const messages = {
  en: {
    ...orders,
  },
};

export default messages;
