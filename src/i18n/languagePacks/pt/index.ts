import orders from './orders';

const messages = {
  ...orders,
};

export default messages;
