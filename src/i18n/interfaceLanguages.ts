export type LanguageName =
  'English' | 'Español'
  | 'Français' | 'Português'
  | '中文（简体)' | '中文（繁體)';

export enum LanguageCode {
  en = 'en',
  es = 'es',
  fr = 'fr',
  pt = 'pt',
  'zh-Hans' = 'zh-Hans',
  'zh-Hant' = 'zh-Hant',
}

export type InterfaceLanguage = {
  name: LanguageName;
  code: LanguageCode;
}

const interfaceLanguages: ReadonlyArray<InterfaceLanguage> = [
  { name: 'English', code: LanguageCode.en },
  { name: 'Español', code: LanguageCode.es },
  { name: 'Français', code: LanguageCode.fr },
  { name: 'Português', code: LanguageCode.pt },
  { name: '中文（简体)', code: LanguageCode['zh-Hans'] },
  { name: '中文（繁體)', code: LanguageCode['zh-Hant'] },
];

export { interfaceLanguages as default };
