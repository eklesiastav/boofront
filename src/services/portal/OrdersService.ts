// import APIClient from '@/services/APIClient';
import { Paginator, Presentation } from '@/types';
import { BasicOrder } from '@/types/orders';
import { OrdersState } from '@/types/store';

// Mocks
import { ordersState } from '@/services/portal/mocks/orders';

// const host = 'https://localhost:8000';

// enum Endpoints {
//   Orders = host + '/orders/',
// }

export default class OrdersService {
  static async fetchOrdersState(
    _page: Paginator<BasicOrder>['currentPage'],
  ): Promise<OrdersState> | never {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(ordersState);
      }, 500);
    });
  }

  static async updatePresentation(
    presentation: Presentation,
  ): Promise<Presentation> | never {
    ordersState.presentation = presentation;
    return Promise.resolve(presentation);
  }
}
