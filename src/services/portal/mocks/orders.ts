import { OrdersState } from '@/types/store';
import { PaymentStatus } from '@/types/orders';

console.log(PaymentStatus);
const ordersState: OrdersState = {
  orders: [
    {
      status: {
        name: 'Delivered Today',
        type: 'delivered',
      },
      details: {
        id: '83620260',
        imgSrc: 'item_1.png',
        title: 'Windproof Touchscreen Glove',
        purchased: '3 months ago',
        storeName: 'Star Buy Store',
        price: 39.99,
        paymentStatus: PaymentStatus[3] = 'Chargeback',
        orderDate: 'Aug 25, 2020 1:17 PM',
      },
    },
    {
      status: {
        name: 'Awaiting Shipment',
        type: 'awaiting',
      },
      details: {
        id: '83620261',
        imgSrc: 'item_3.png',
        title: 'Anti-Slip Shoe Grips',
        purchased: 'Today',
        storeName: 'Star Buy Store',
        price: 37.99,
        paymentStatus: PaymentStatus[5],
        orderDate: 'Aug 25, 2018 5:17 PM',
      },
    },
    {
      status: {
        name: 'Delivered',
        type: 'delivered',
      },
      details: {
        id: '83620262',
        imgSrc: 'item_1.png',
        title: 'Swarm Software',
        purchased: '3 months ago',
        storeName: 'Star Buy Store',
        price: 89.99,
        paymentStatus: '',
        orderDate: 'Aug 25, 2018 5:17 PM',
      },
    },
    {
      status: {
        name: 'Arriving Tomorrow',
        type: 'arriving',
      },
      details: {
        id: '83620263',
        imgSrc: 'item_1.png',
        title: 'Windproof Touchscreen Glove',
        purchased: '3 months ago',
        storeName: 'Star Buy Store',
        price: 59.99,
        paymentStatus: PaymentStatus[4],
        orderDate: 'Aug 26, 2018 5:17 PM',
      },
    },
    {
      status: {
        name: 'Incomplete Purchase',
        type: 'incomplete',
      },
      details: {
        id: '83620264',
        imgSrc: 'item_2.png',
        title: 'Anti-Slip Shoe Grips',
        purchased: '3 months ago',
        storeName: 'Star Buy Store',
        paymentStatus: '',
        price: 15,
        orderDate: 'Aug 28, 2019 5:17 PM',
      },
    },
    {
      status: {
        name: 'Awaiting Pickup',
        type: 'awaiting_pickup',
      },
      details: {
        id: '83620265',
        imgSrc: 'item_4.png',
        title: 'Cordless Angle Grinding',
        purchased: '2 years ago',
        storeName: 'Star Buy Store',
        paymentStatus: '',
        price: 40,
        orderDate: 'Sep 25, 2018 5:17 PM',
      },
    },
  ],
  totalOrders: 4,
  pagination: { currentPage: 1, totalPages: 1, pageSize: 25 },
  presentation: {
    layout: 'grid',
    ordering: 'asc',
    sortBy: { key: 'orderDate', display: 'order date' },
  },
};

const order = [
  {
    customer: {
      id: 1,
      firstName: 'vasya',
      lastName: 'Pupkin',
    },
    total: 1,
    paymentStatus: 'completed',
    fulfillmentStatus: 'sdfsdf',
    deliveryStatus: 'delivered',
  },
];

export { ordersState, order };
