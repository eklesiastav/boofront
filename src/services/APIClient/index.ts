/* eslint consistent-return: 0 */

import axios, { AxiosError } from 'axios';
import { LanguageCode } from '@/i18n/interfaceLanguages';
import {
  UnauthorizedAPIServiceError,
  UnauthorizedIPError,
  UnapprovedWorkScheduleError,
  SuspendedAccountError,
  ResourceNotFound,
} from '@/exceptions';
import TokenService from '@/services/authentication/TokenService';
import CustomApiErrorCodes from './CustomApiErrorCodes';

class APIClient {
  static init(baseURL: string) {
    axios.defaults.baseURL = baseURL;
  }

  static setTokenHeader() {
    axios.defaults.headers.common.Authorization = `Token ${TokenService.getToken()}`;
  }

  static setLanguageHeader(lang: LanguageCode) {
    axios.defaults.headers.common['Accept-Language'] = lang;
  }

  static setHeaders(header: { [key: string]: string }) {
    /* eslint no-restricted-syntax: 0 */
    for (const key of Object.keys(header)) {
      axios.defaults.headers.common[key] = header[key];
    }
  }

  static removeAllHeaders() {
    axios.defaults.headers.common = {};
  }

  static async get(resource: string, params: object | null = null) {
    try {
      return axios.get(resource, { params });
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async post(resource: string, data: object | null) {
    try {
      return axios.post(resource, data);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async put(resource: string, data: object) {
    try {
      return axios.put(resource, data);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async patch(resource: string, data: object) {
    try {
      return axios.patch(resource, data);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async delete(resource: string) {
    try {
      return axios.delete(resource);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async customRequest(data: object) {
    try {
      return axios(data);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static async all(data: object[]) {
    try {
      return Promise.all(data);
    } catch (error) {
      this.APIErrorInterceptor(error);
    }
  }

  static APIErrorInterceptor(error: AxiosError): never {
    switch (error.response?.data.errorCode) {
      case CustomApiErrorCodes.UnauthorizedService:
        throw new UnauthorizedAPIServiceError(+error.response.status, error.response.data);
      case CustomApiErrorCodes.ResourceNotFound:
        throw new ResourceNotFound(+error.response.status, error.response.data);
      case CustomApiErrorCodes.UnauthorizedIp:
        throw new UnauthorizedIPError(+error.response.status, error.response.data);
      case CustomApiErrorCodes.UnapprovedWorkSchedule:
        throw new UnapprovedWorkScheduleError(+error.response.status, error.response.data);
      case CustomApiErrorCodes.SuspendedAccount:
        throw new SuspendedAccountError(+error.response.status, error.response.data);
      default:
        throw error;
    }
  }
}

export default APIClient;
