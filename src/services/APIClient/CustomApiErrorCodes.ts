enum CustomApiErrorCodes {
  // Authentication Error Codes
  IncorrectPassword = 10000,
  UnauthorizedService = 10001,
  UnauthorizedIp = 10002,
  UnapprovedWorkSchedule = 10003,
  SuspendedAccount = 10004,
  PausedAccount = 10005,
  DeletedAccountProbation = 10006,
  PasswordReuse = 10007,
  // Resource/Entity Error Codes
  DuplicateEntity = 10008,
  ResourceNotFound = 404,
}

export default CustomApiErrorCodes;
