import APIClient from '../APIClient';
import TokenService from './TokenService';

// const host = 'https://localhost:8000';

enum UserEndpoints {
  SignOut = '/auth/logout/',
}

class UserService {
  static async signOut() {
    if (!TokenService.getToken()) {
      return;
    }
    const resource = UserEndpoints.SignOut;
    const data = null;

    await APIClient.post(resource, data);

    TokenService.removeToken();
    APIClient.removeAllHeaders();
  }

  static async getRoleData() {
    //
  }
}

export default UserService;
