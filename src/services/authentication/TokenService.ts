class TokenService {
  private static readonly TOKEN_KEY = 'token';

  static getToken() {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  static saveToken(token: string) {
    localStorage.setItem(this.TOKEN_KEY, token);
  }

  static removeToken() {
    localStorage.removeItem(this.TOKEN_KEY);
  }
}

export default TokenService;
