// import APIClient from '@/services/APIClient';

// const host = 'https://localhost:8000';

// enum Endpoints {
//   Customers = '/customers/',
// }

import { AccountInformation, BusinessInformation, Role } from '@/types';
import { RootState } from '@/types/store';

const mockAccountData: Pick<RootState, 'accountInformation' | 'workSchedule' | 'suspension'> = {
  accountInformation: {
    id: '12',
    emailAddress: 'useremail@email.com',
    firstName: 'Marcus',
    lastName: 'Steph',
    avatarURL: 'https://bit.ly/2GTItJp',
    dateJoined: '2020-08-18T21:07:25.089367-04:00',
    lastLogin: '2020-04-18T21:07:25.089367-04:00',
    isPhoneVerified: false,
    isOwner: true,
    isActive: true,
    roleId: '0',
    streetAddress: '300 E Chestnut Street',
    apartment: '',
    country: 'United States',
    region: '',
    city: '',
    zip: '',
    phoneCountry: 'United States',
    phoneNumber: '',
    accountTimezone: 'America/Chicago',
    lastRejectedTimezone: '',
    accountLanguage: 'en',
    locale: 'en-US',
    colorScheme: 'light',
    lastRejectedColorScheme: '',
  },

  workSchedule: {
    startDate: '2020-03-18T21:07:25.089367-05:00',
    endDate: '2020-06-15T20:00:25.089367-05:00',
    workTime: [
      { day: 'Sunday', isActive: false, hours: { start: '9:40 AM', stop: '5:00 PM' } },
      { day: 'Monday', isActive: true, hours: { start: '9:23 AM', stop: '8:31 PM' } },
      { day: 'Tuesday', isActive: true, hours: { start: '9:00 AM', stop: '5:00 PM' } },
      { day: 'Wednesday', isActive: true, hours: { start: '9:00 AM', stop: '5:00 PM' } },
      { day: 'Thursday', isActive: true, hours: { start: '9:00 AM', stop: '5:00 PM' } },
      { day: 'Friday', isActive: true, hours: { start: '9:00 AM', stop: '5:00 PM' } },
      { day: 'Saturday', isActive: false, hours: { start: '9:00 AM', stop: '5:00 PM' } },
    ],
    isWorkHoursEnabled: true,
    isWorkStartEndDateEnabled: false,
  },

  suspension: {
    startDate: '2020-08-18T21:07:25.089367-04:00',
    endDate: '2020-09-02T21:07:25.089367-04:00',
  },
};

const mockBusinessInformation: BusinessInformation = {
  id: '3',
  companyName: 'BigCommand LLC',
  website: '',
  logoURL: 'https://bit.ly/312raMW',
  streetAddress: '',
  suite: '',
  country: 'United States',
  region: '',
  city: '',
  zip: '',
  phoneCountry: 'United States',
  phoneNumber: '',
  phoneExtension: '',
  accountCurrency: { id: 1, name: 'US Dollar - USD ($)' },
  hasMadeFirstSale: false,
};

const mockAccountRole: Role = {
  id: '16',
  name: 'Manager',
  isActive: true,
  creationDate: '2020-04-18T21:07:25.089367-04:00',
  totalSubusers: 0,
  apps: [
    { id: 1, name: 'Home', permissions: [] },
    {
      id: 2,
      name: 'Account',
      permissions: ['Can create BigPixel tracking code', 'Can remove BigPixel tracking code'],
    },
    { id: 11, name: 'Help Center', permissions: [] },
    { id: 3, name: 'BoomFront', permissions: ['can_import_products', 'can_export_products'] },
  ],
};

class AccountInformationService {
  static async fetchAccountData():
    Promise<Pick<RootState, 'accountInformation' | 'workSchedule' | 'suspension'>> | never {
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mockAccountData); }, 500);
    });
  }

  static async fetchBusinessInformation(): Promise<BusinessInformation> | never {
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mockBusinessInformation); }, 500);
    });
  }

  static async fetchAccountRole(): Promise<Role> | never {
    return new Promise((resolve) => {
      setTimeout(() => { resolve(mockAccountRole); }, 500);
    });
  }

  static async updateTimezone(
    _newTimezone: AccountInformation['accountTimezone'],
  ): Promise<AccountInformation> | never {
    return Promise.resolve(mockAccountData.accountInformation);
  }

  static async updateLastRejectedTimezone(
    _rejectedTimezone: AccountInformation['accountTimezone'],
  ): Promise<AccountInformation> | never {
    return Promise.resolve(mockAccountData.accountInformation);
  }

  static async updateColorScheme(
    _newColorScheme: 'light' | 'dark',
  ): Promise<AccountInformation> | never {
    return Promise.resolve(mockAccountData.accountInformation);
  }

  static async updateLastRejectedColorScheme(
    _rejectedColorScheme: 'light' | 'dark',
  ): Promise<AccountInformation> | never {
    return Promise.resolve(mockAccountData.accountInformation);
  }
}

export default AccountInformationService;
