// TODO: API update for recent apps instead of local storage.
import { map, isEmpty } from 'lodash';
import bigCommandApps from '@/includes/common/bigCommandApps';
import { BigCommandApp } from '@/types';

/* eslint class-methods-use-this:0 */
class BigCommandAppService {
  /**
   * Adds an app to the recently used apps array and saves to local storage.
   * @param {number} id
   */
  static updateRecentApps(id: BigCommandApp['id']) {
    let recentApps = map(this.getRecentApps(), 'id');
    if (!isEmpty(recentApps)) {
      if (recentApps.includes(id)) {
        const index = recentApps.indexOf(id);
        recentApps.unshift(recentApps.splice(index, 1)[0]);
      } else {
        recentApps.unshift(id);
      }
    } else {
      recentApps = [id];
    }

    this.setRecentApps(recentApps);
  }

  /**
   * Save 5 most recently used apps to local storage.
   * @param recentApps
   */
  static setRecentApps(recentApps: BigCommandApp['id'][]) {
    localStorage.setItem('recentApps', JSON.stringify(recentApps.slice(0, 5)));
  }

  /**
   * Get the users most-recently used applications from local storage if they
   * exists.
   * @returns {Array.{Object}}
   */
  static getRecentApps() {
    const recentAppsList = localStorage.getItem('recentApps');
    const allAppData: BigCommandApp[] = [];
    if (recentAppsList) {
      const apps = JSON.parse(recentAppsList) as BigCommandApp['id'][];
      apps.forEach((id) => {
        const appData = bigCommandApps.find((item) => item.id === id);
        allAppData.push(appData as BigCommandApp);
      });
    }
    return allAppData;
  }
}

export default BigCommandAppService;
