import { ActionTree } from 'vuex';
import { SettingsStore, RootState } from '@/types/store';

const actions: ActionTree<SettingsStore, RootState> = {};

export default actions;
