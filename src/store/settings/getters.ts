import { GetterTree } from 'vuex';
import { SettingsStore, RootState } from '@/types/store';

const getters: GetterTree<SettingsStore, RootState> = {};

export default getters;
