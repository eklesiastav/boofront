import { Module } from 'vuex';
import { SettingsStore, RootState } from '@/types/store';
import state from '@/store/settings/state';
import getters from '@/store/settings/getters';
import mutations from '@/store/settings/mutations';
import actions from '@/store/settings/actions';

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
} as Module<SettingsStore, RootState>;
