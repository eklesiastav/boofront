import { MutationTree } from 'vuex';
import { SettingsStore } from '@/types/store';

const mutations: MutationTree<SettingsStore> = {};

export default mutations;
