import { GetterTree } from 'vuex';
import { EventsStore, RootState } from '@/types/store';

const getters: GetterTree<EventsStore, RootState> = {};

export default getters;
