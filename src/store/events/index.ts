import { Module } from 'vuex';
import { EventsStore, RootState } from '@/types/store';
import state from '@/store/events/state';
import getters from '@/store/events/getters';
import mutations from '@/store/events/mutations';
import actions from '@/store/events/actions';

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
} as Module<EventsStore, RootState>;
