import { ActionTree } from 'vuex';
import { EventsStore, RootState } from '@/types/store';

const actions: ActionTree<EventsStore, RootState> = {};

export default actions;
