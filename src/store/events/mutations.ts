import { MutationTree } from 'vuex';
import { EventsStore } from '@/types/store';

const mutations: MutationTree<EventsStore> = {};

export default mutations;
