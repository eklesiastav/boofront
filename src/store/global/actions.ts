import { ActionTree } from 'vuex';
import { RootState } from '@/types/store';
import { AccountInformation, UserFeedback } from '@/types';
import AccountInformationService from '@/services/account/AccountInformationService';

const FEEDBACK_RESIDENCE_TIME = 4000;

const actions: ActionTree<RootState, RootState> = {
  async fetchAccountData({ commit }) {
    const accountData = await AccountInformationService.fetchAccountData();
    commit('updateAccountData', accountData);
    return Promise.resolve();
  },
  async fetchBusinessInformation({ commit }) {
    const businessInformation = await AccountInformationService.fetchBusinessInformation();
    commit('updateBusinessInformation', businessInformation);
    return Promise.resolve();
  },
  async fetchAccountRole({ commit }) {
    const role = await AccountInformationService.fetchAccountRole();
    commit('updateAccountRole', role);
    return Promise.resolve();
  },
  async updateTimezone({ commit }, newTimezone: AccountInformation['accountTimezone']) {
    await AccountInformationService.updateTimezone(newTimezone);
    commit('updateTimezone', newTimezone);
    return Promise.resolve();
  },
  async updateLastRejectedTimezone(
    { commit },
    rejectedTimezone: AccountInformation['accountTimezone'],
  ) {
    await AccountInformationService.updateLastRejectedTimezone(rejectedTimezone);
    commit('updateLastRejectedTimezone', rejectedTimezone);
    return Promise.resolve();
  },
  async updateColorScheme({ commit }, newColorScheme: 'light' | 'dark') {
    await AccountInformationService.updateColorScheme(newColorScheme);
    commit('updateColorScheme', newColorScheme);
    document.body.style.background = newColorScheme === 'dark' ? '#000E12' : '#F9FBFB';
    return Promise.resolve();
  },
  async updateLastRejectedColorScheme({ commit }, rejectedColorScheme: 'light' | 'dark') {
    await AccountInformationService.updateLastRejectedColorScheme(rejectedColorScheme);
    commit('updateLastRejectedColorScheme', rejectedColorScheme);
    return Promise.resolve();
  },
  showFeedback({ commit }, feedbackDetail: UserFeedback) {
    commit('showFeedback', feedbackDetail);
    setTimeout(() => {
      commit('closeFeedbackWidget');
    }, FEEDBACK_RESIDENCE_TIME);
  },
};

export default actions;
