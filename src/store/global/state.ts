import { RootState } from '@/types/store';

const state = {
  accountInformation: {
    id: '',
    emailAddress: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    dateJoined: '',
    lastLogin: '',
    isPhoneVerified: false,
    isOwner: false,
    isActive: false,
    roleId: '',
    streetAddress: '',
    apartment: '',
    country: '',
    region: '',
    city: '',
    zip: '',
    phoneCountry: '',
    phoneNumber: '',
    accountTimezone: '',
    lastRejectedTimezone: '',
    accountLanguage: 'en',
    locale: 'en-US',
    colorScheme: 'light',
    lastRejectedColorScheme: '',
  },

  workSchedule: {
    startDate: '',
    endDate: '',
    workTime: [],
    isWorkHoursEnabled: false,
    isWorkStartEndDateEnabled: false,
  },

  suspension: {
    startDate: '',
    endDate: '',
  },

  businessInformation: {
    id: '',
    companyName: '',
    website: '',
    logoURL: '',
    streetAddress: '',
    suite: '',
    country: '',
    region: '',
    city: '',
    zip: '',
    phoneCountry: '',
    phoneNumber: '',
    phoneExtension: '',
    accountCurrency: { id: 1, name: 'US Dollar - USD ($)' },
    hasMadeFirstSale: true,
  },

  accountRole: {
    id: '',
    name: '',
    isActive: false,
    creationDate: '',
    totalSubusers: 0,
    apps: [],
  },

  feedback: {
    isFeedbackWidgetVisible: false,
    isPermissionDeniedModalVisible: false,
    type: 'success',
    title: 'Feedback message title',
    description: 'Feedback message description.',
  },

  videoGuide: {
    isVideoGuideWidgetVisible: false,
    videoURL: '',
    guideTitle: '',
    guideDescription: '',
  },
} as RootState;

export default state;
