import { MutationTree } from 'vuex';
import { RootState } from '@/types/store';
import {
  BusinessInformation,
  Role,
  UserFeedback,
} from '@/types';
import { ValueError } from '@/exceptions';
import videoGuides from '@/includes/common/videoGuides';

const mutations: MutationTree<RootState> = {
  updateAccountData(
    state: RootState,
    accountData: Pick<RootState, 'accountInformation' | 'workSchedule' | 'suspension'>,
  ) {
    state.accountInformation = accountData.accountInformation;
    state.workSchedule = accountData.workSchedule;
    state.suspension = accountData.suspension;
  },
  updateBusinessInformation(state: RootState, businessInformation: BusinessInformation) {
    state.businessInformation = businessInformation;
  },
  updateAccountRole(state: RootState, accountRole: Role) {
    state.accountRole = accountRole;
  },
  updateTimezone(state: RootState, newTimezone: string) {
    (state as any).accountInformation.accountTimezone = newTimezone;
  },
  updateLastRejectedTimezone(state: RootState, rejectedTimezone: string) {
    (state as any).accountInformation.lastRejectedTimezone = rejectedTimezone;
  },
  updateColorScheme(state: RootState, newColorScheme: 'light' | 'dark') {
    (state as any).accountInformation.colorScheme = newColorScheme;
  },
  updateLastRejectedColorScheme(state: RootState, rejectedColorScheme: 'light' | 'dark') {
    (state as any).accountInformation.lastRejectedColorScheme = rejectedColorScheme;
  },
  showFeedback(state: RootState, feedbackDetail: UserFeedback) {
    state.feedback.isFeedbackWidgetVisible = true;
    state.feedback.type = feedbackDetail.type;
    state.feedback.title = feedbackDetail.title ?? 'Cannot continue';
    state.feedback.description = feedbackDetail.description ?? 'Please fix the error and try again';
  },
  closeFeedbackWidget(state: RootState) {
    state.feedback.isFeedbackWidgetVisible = false;
  },
  playVideoGuide(state: RootState, guideName: string) {
    const guide = videoGuides
      .find((item) => item.name === guideName);
    if (!guide) throw new ValueError('Guide not found.');
    state.videoGuide.isVideoGuideWidgetVisible = true;
    state.videoGuide.videoURL = guide.data.videoURL;
    state.videoGuide.guideTitle = guide.data.guideTitle;
    state.videoGuide.guideDescription = guide.data.guideDescription;
  },
  closeVideoGuide(state: RootState) {
    state.videoGuide.isVideoGuideWidgetVisible = false;
    state.videoGuide.videoURL = '';
    state.videoGuide.guideTitle = '';
    state.videoGuide.guideDescription = '';
  },
  showPermissionDeniedModal(state: RootState) {
    state.feedback.isPermissionDeniedModalVisible = true;
  },
  hidePermissionDeniedModal(state: RootState) {
    state.feedback.isPermissionDeniedModalVisible = false;
  },
};

export default mutations;
