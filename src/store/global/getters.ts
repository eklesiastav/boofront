import { GetterTree } from 'vuex';
import { RootState } from '@/types/store';
import { getCurrencySymbol } from '@/includes/common/currencies/currencyUtils';

const getters: GetterTree<RootState, RootState> = {
  fullName: (state: RootState) => `${state.accountInformation.firstName}
  ${state.accountInformation.lastName}`,

  unitSystem: (state: RootState) => {
    if (state.accountInformation.locale.includes('US')) return 'Imperial';
    return 'Metric';
  },

  currencySymbol: (state: RootState) => (
    getCurrencySymbol(state.businessInformation.accountCurrency.name)
  ),
};

export default getters;
