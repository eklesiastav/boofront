import Vue from 'vue';
import Vuex from 'vuex';
import { RootState } from '@/types/store';
import orders from '@/store/orders';
import subscriptions from '@/store/subscriptions';
import events from '@/store/events';
import fastSign from '@/store/fastSign';
import settings from '@/store/settings';
import global from './global';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: { ...global.state } as RootState,
  getters: { ...global.getters },
  mutations: { ...global.mutations },
  actions: { ...global.actions },
  modules: {
    orders,
    subscriptions,
    events,
    fastSign,
    settings,
  },
});
