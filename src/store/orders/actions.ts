import { ActionTree } from 'vuex';
import { OrdersState, RootState } from '@/types/store';
import OrdersService from '@/services/portal/OrdersService';
import { Presentation } from '@/types';

const actions: ActionTree<OrdersState, RootState> = {
  async fetchOrdersState({ commit, state }, page = 1, forceFetch = false) {
    // Page has already been fetched
    if (state.pagination.currentPage === page && state.lastFetchedTimestamp && !forceFetch) {
      return Promise.resolve();
    }

    // Fetch page
    const ordersState = await OrdersService.fetchOrdersState(page);
    commit('updateOrdersState', ordersState);
    commit('updateLastFetchedTimestamp', new Date().toJSON());

    return Promise.resolve();
  },
  async updatePresentation({ commit }, presentation: Presentation) {
    const updated = await OrdersService.updatePresentation(presentation);
    commit('updatePresentation', updated);
    return Promise.resolve();
  },
};

export default actions;
