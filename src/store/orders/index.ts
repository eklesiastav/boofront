import { Module } from 'vuex';
import { OrdersState, RootState } from '@/types/store';
import state from '@/store/orders/state';
import getters from '@/store/orders/getters';
import mutations from '@/store/orders/mutations';
import actions from '@/store/orders/actions';

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
} as Module<OrdersState, RootState>;
