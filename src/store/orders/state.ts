import { OrdersState } from '@/types/store';

const state: OrdersState = {
  orders: [],
  totalOrders: 0,
  pagination: { currentPage: 1, totalPages: 1, pageSize: 25 },
  presentation: {
    layout: 'grid',
    ordering: 'asc',
    sortBy: { key: 'orderDate', display: 'order date' },
  },
  lastFetchedTimestamp: '',
};

export default state;
