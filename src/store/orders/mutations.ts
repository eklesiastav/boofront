import { MutationTree } from 'vuex';
import { OrdersState } from '@/types/store';
import { Presentation } from '@/types';

const mutations: MutationTree<OrdersState> = {
  updateOrdersState(state, ordersState: OrdersState) {
    state.orders = ordersState.orders;
    state.totalOrders = ordersState.totalOrders;
    state.pagination = ordersState.pagination;
  },
  updateLastFetchedTimestamp(state, timestamp: string) {
    state.lastFetchedTimestamp = timestamp;
  },
  updatePresentation(state, presentation: Presentation) {
    state.presentation = presentation;
  },
};

export default mutations;
