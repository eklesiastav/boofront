import { GetterTree } from 'vuex';
import { OrdersState, RootState } from '@/types/store';

const getters: GetterTree<OrdersState, RootState> = {};

export default getters;
