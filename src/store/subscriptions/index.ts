import { Module } from 'vuex';
import { SubscriptionsState, RootState } from '@/types/store';
import state from '@/store/subscriptions/state';
import getters from '@/store/subscriptions/getters';
import mutations from '@/store/subscriptions/mutations';
import actions from '@/store/subscriptions/actions';

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
} as Module<SubscriptionsState, RootState>;
