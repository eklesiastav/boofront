import { GetterTree } from 'vuex';
import { SubscriptionsState, RootState } from '@/types/store';

const getters: GetterTree<SubscriptionsState, RootState> = {};

export default getters;
