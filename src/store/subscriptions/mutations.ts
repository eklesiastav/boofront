import { MutationTree } from 'vuex';
import { SubscriptionsState } from '@/types/store';

const mutations: MutationTree<SubscriptionsState> = {};

export default mutations;
