import { ActionTree } from 'vuex';
import { SubscriptionsState, RootState } from '@/types/store';

const actions: ActionTree<SubscriptionsState, RootState> = {};

export default actions;
