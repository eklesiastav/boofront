import { MutationTree } from 'vuex';
import { FastSignStore } from '@/types/store';

const mutations: MutationTree<FastSignStore> = {};

export default mutations;
