import { Module } from 'vuex';
import { FastSignStore, RootState } from '@/types/store';
import state from '@/store/fastSign/state';
import getters from '@/store/fastSign/getters';
import mutations from '@/store/fastSign/mutations';
import actions from '@/store/fastSign/actions';

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
} as Module<FastSignStore, RootState>;
