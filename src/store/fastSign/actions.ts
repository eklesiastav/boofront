import { ActionTree } from 'vuex';
import { FastSignStore, RootState } from '@/types/store';

const actions: ActionTree<FastSignStore, RootState> = {};

export default actions;
