import { GetterTree } from 'vuex';
import { FastSignStore, RootState } from '@/types/store';

const getters: GetterTree<FastSignStore, RootState> = {};

export default getters;
