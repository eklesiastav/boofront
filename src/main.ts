import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import i18n from './i18n/setup';
import initializeVueApp from './includes/common/vue/initializeVueApp';
import registerGlobalComponents from './components/registerGlobalComponents';
import registerGlobalFilters from './includes/common/vue/registerGlobalFilters';
import registerGlobalDirectives from './includes/common/vue/registerGlobalDirectives';

initializeVueApp(Vue, store);
registerGlobalComponents(Vue);
registerGlobalFilters(Vue);
registerGlobalDirectives(Vue);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
