/* eslint no-useless-constructor: 0 */
/* eslint max-classes-per-file: 0 */

abstract class UnAuthorizedAccessError extends Error {
  protected constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

/**
 * Thrown when an account subuser tries to perform an action they have not been approved for.
 * The error is handled globally through Vue error handler by showing a feedback modal window.
 */
class PermissionDeniedError extends UnAuthorizedAccessError {
  constructor(message: string) {
    super(message);
  }
}

/**
 * Thrown when a user tries to access a resource they do not have permission to.
 * This is used to handle backend access denied exception.
 * The error is handled globally by Vue error handler and redirects the user to a
 * ResourceNotFound page.
 */
class UnauthorizedAPIServiceError extends UnAuthorizedAccessError {
  constructor(public httpStatusCode: number, message: string) {
    super(message);
    this.httpStatusCode = httpStatusCode;
  }
}

export { PermissionDeniedError, UnauthorizedAPIServiceError };
