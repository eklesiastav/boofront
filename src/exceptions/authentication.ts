/* eslint no-useless-constructor: 0 */
/* eslint max-classes-per-file: 0 */

abstract class AuthenticationError extends Error {
  protected constructor(public httpStatusCode: number, message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

/**
 * Thrown when during sign in if the user-provided password is incorrect.
 */
class IncorrectPasswordError extends AuthenticationError {
  constructor(httpStatusCode: number, message: string) {
    super(httpStatusCode, message);
  }
}

/**
 * Thrown if a user is accessing any BigCommand app from an IP address that has not been
 * approved by the account owner. See Security component for IP restriction feature.
 * The error is handled globally by Vue error handler and redirects the user to a
 * UnauthorizedIP page.
 */
class UnauthorizedIPError extends AuthenticationError {
  constructor(httpStatusCode: number, message: string) {
    super(httpStatusCode, message);
  }
}

/**
 * Thrown if a user is accessing any BigCommand app outside the approved daily work hours or
 * work start and end dates.
 * The error is handled globally by Vue error handler and redirects the user to a
 * UnapprovedWorkSchedule page.
 */
class UnapprovedWorkScheduleError extends AuthenticationError {
  constructor(httpStatusCode: number, message: string) {
    super(httpStatusCode, message);
  }
}

/**
 * Thrown when a user tries to access a suspended BigCommand account.
 * The error is handled globally by Vue error handler and redirects the user to a
 * SuspendedAccount page.
 */
class SuspendedAccountError extends AuthenticationError {
  constructor(httpStatusCode: number, message: string) {
    super(httpStatusCode, message);
  }
}

export {
  IncorrectPasswordError,
  UnauthorizedIPError,
  UnapprovedWorkScheduleError,
  SuspendedAccountError,
};
