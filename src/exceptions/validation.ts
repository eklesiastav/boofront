/* eslint no-useless-constructor: 0 */
/* eslint max-classes-per-file: 0 */

abstract class ValidationError extends Error {
  protected constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

class ValueError extends Error {
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

/**
 * Thrown when data to validate is not of a given type.
 * Used by the data type validation module.
 */
class InvalidTypeError extends ValidationError {
  constructor(message: string) {
    super(message);
  }
}

/**
 * Thrown when an object doesnt have the property keys to be validated.
 * Used by the data type validation module.
 */
class MissingPropertyError extends ValidationError {
  constructor(message: string) {
    super(message);
  }
}

/**
 * Thrown when the data to validate is empty.
 * Used by the user input validation module.
 */
class EmptyDataError extends ValidationError {
  constructor(message: string) {
    super(message);
  }
}

/**
 * Thrown when the data to validate in invalid.
 * Used by the user input validation module.
 */
class InvalidDataError extends ValidationError {
  constructor(message: string) {
    super(message);
  }
}

export {
  InvalidDataError,
  ValueError,
  InvalidTypeError,
  MissingPropertyError,
  EmptyDataError,
};
