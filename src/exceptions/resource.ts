/**
 * Thrown when accessing non-existing resource
 */
export default class ResourceNotFound extends Error {
  constructor(public httpStatusCode: number, message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}
