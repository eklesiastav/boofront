import {
  AccountInformation,
  Role,
  BusinessInformation, Paginator, Presentation,
  UserFeedback,
  VideoGuide, WorkSchedule, AccountSuspension,
} from '@/types/index';
import { BasicOrder } from '@/types/orders';

export type StateCommons = {
  pagination: Pick<Paginator<any>, 'currentPage' | 'totalPages' | 'pageSize'>;
  presentation: Presentation;
  lastFetchedTimestamp?: string;
}

export type RootState = {
  accountInformation: AccountInformation;
  workSchedule: WorkSchedule;
  suspension: AccountSuspension;
  businessInformation: BusinessInformation;
  accountRole: Role;
  feedback: UserFeedback;
  videoGuide: VideoGuide;
};

export type AuthenticationState = {
  readonly data?: unknown;
};

export type OrdersState = StateCommons & {
  orders: BasicOrder[];
  totalOrders: number;
};

export type SubscriptionsState = {
  readonly data?: unknown;
};

export type EventsStore = {
  readonly data?: unknown;
};

export type FastSignStore = {
  readonly data?: unknown;
};

export type SettingsStore = {
  readonly data?: unknown;
};
