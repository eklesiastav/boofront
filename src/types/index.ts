export type AccountInformation = {
  readonly id: string;
  readonly emailAddress: string;
  firstName: string;
  lastName: string;
  readonly avatarURL: string;
  readonly dateJoined: string;
  readonly lastLogin: string;
  isPhoneVerified: boolean;
  readonly isOwner: boolean;
  isActive: boolean;
  roleId: string;
  streetAddress: string;
  apartment: string;
  country: string;
  region: string;
  city: string;
  zip: string;
  phoneCountry: string;
  phoneNumber: string;
  accountTimezone: string;
  lastRejectedTimezone: string;
  accountLanguage: string;
  locale: string;
  colorScheme: 'light' | 'dark';
  lastRejectedColorScheme: 'light' | 'dark' | '';
};

export type WorkSchedule = {
  startDate: string | null;
  endDate: string | null;
  workTime: WorkTime[];
  isWorkHoursEnabled: boolean;
  isWorkStartEndDateEnabled: boolean;
};

type WorkTime = { day: string; isActive: boolean; hours: { start: string; stop: string } };

export type AccountSuspension = {
  readonly startDate: string;
  readonly endDate: string;
  readonly suspendedBy?: AccountInformation['id'];
  readonly unsuspendedBy?: AccountInformation['id'];
};

export type Role = {
  readonly id: string;
  readonly name: string;
  readonly isActive: boolean;
  readonly apps: {
    id: BigCommandApp['id'];
    name: BigCommandApp['name'];
    permissions: Array<Permission['codeName']>;
  }[];

  [propName: string]: any;
};

export type BusinessInformation = {
  readonly id: string;
  companyName: string;
  website: string;
  logoURL: string;
  accountCurrency: Currency;
  streetAddress: string;
  suite: string;
  country: string;
  region: string;
  city: string;
  zip: string;
  phoneCountry: string;
  phoneNumber: string;
  phoneExtension: string;
  readonly hasMadeFirstSale: boolean;
};

export type BigCommandApp = {
  readonly id: number;
  readonly name: string;
  readonly description: string;
  readonly logo: string;
  readonly alt: string;
  readonly route?: { name: string };
  readonly link?: string;
  readonly isPaid: boolean;
};

export type InternalNavRoute = {
  readonly name: string;
  readonly route?: string;
  readonly param?: { name: string; value: string | number };
  readonly path?: string;
};

export type MenuOption<T> = {
  readonly name: string;
  readonly action: (arg?: T) => void;
  readonly actionParam?: T;
  readonly icon?: string;
  readonly isDisabled?: boolean;
};

export type Permission = {
  readonly name: string;
  readonly codeName: string;
  readonly description?: string;
};

export type PermissionCategory = {
  readonly category: string;
  readonly permissions: Permission[];
};

export type UserFeedback = {
  type: 'success' | 'failure';
  title: string;
  description: string;

  [propName: string]: any;
};

export type VideoGuide = {
  videoURL: string;
  guideTitle: string;
  guideDescription: string;

  [propName: string]: any;
};

export type InputValidityObject = {
  isValid: boolean;
  errorMessage: string;
};

export type Currency = {
  id: number;
  name: string;
};

export enum Months {
  January,
  February,
  March,
  April,
  May,
  June,
  July,
  August,
  September,
  October,
  November,
  December,
}

export enum Days {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
}

export type Note = {
  id: number | null;
  note: string;
  readonly createdBy?: AccountInformation['id'];
  readonly lastModifiedBy?: AccountInformation['id'];
  readonly creationDate: string; // JSON Date String
  readonly lastModifiedDate: string; // JSON Date String
};

export interface Paginator<T> {
  hasNext: boolean;
  hasPrevious: boolean;
  currentPage: number;
  totalPages: number;
  pageSize: 10 | 25 | 50 | 100;
  fetchNext(): Array<T>;
  fetchPrevious(): Array<T>;
  goToPage(): Array<T>;
}
export interface Copier {
  copyToClipboard(data: { name: string; value: string }): void;
}

export interface LayoutComposer {
  setLayout(layout: Presentation['layout']): void;
}

export interface OrderSorter {
  sortAndOrderBy(orderSort: Pick<Presentation, 'ordering' | 'sortBy'>): void;
}

export interface SearchFilter<T> {
  searchPayload: string;
  filter?: Array<T>;
  search?(): Array<T>;
}

export type Presentation = {
  layout?: 'list' | 'grid';
  sortBy?: { key: string; display: string };
  ordering?: 'asc' | 'desc';
}

export enum LogLevel {
  Debug = 1,
  Info,
  Warn,
  Error,
  Fatal,
}
