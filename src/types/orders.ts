import { Customer } from '@/types/contracts';
import {
  Coupon, CouponType, Product, ProductType,
} from '@/types/products';

import { AccountInformation, Note } from '@/types';

export type Order<T extends ProductType<T> > = {
  readonly id: string;
  customer: Customer;
  items: {
    product: Product<ProductType<T>>;
    quantity: number;
  }[];
  subTotal: number;
  shipping: number;
  tax: number;
  discount: Coupon<CouponType>;
  readonly orderDate: string; // JSON Date String
  payment: OrderPayment;
  fulfillment: T extends 'digital' ? DigitalOrderFulfillment : PhysicalOrderFulfillment;
  delivery: T extends 'digital' ? DigitalOrderDelivery : PhysicalOrderDelivery;
  notes: Note[];
};

export type BasicOrder = Pick<Order<any>, 'id' | 'orderDate'> &
  {
    status: {
      readonly name: string;
      readonly type: string;
    };
    details: {
      readonly id: string;
      readonly imgSrc: string;
      readonly title: string;
      readonly purchased: string;
      readonly storeName: string;
      readonly price: number;
      readonly orderDate: string;
      paymentStatus: PaymentStatus;
    };
  };

export type OrderSummary = {
  paymentStatus: PaymentStatus;
  fulfillmentStatus: FulfillmentStatus;
  deliveryStatus: DeliveryStatus;
  imgSrc: string;
  title: string;
  purchased: string;
  storeName: string;
  price: number;
  orderInfo: string;
  disputeLink: string;
}

export type ManualOrder = Omit<Order<ProductType<any>>, 'orderDate'> & {
  readonly createdBy?: AccountInformation['id'];
  readonly lastModifiedBy?: AccountInformation['id'];
  readonly creationDate: string; // JSON Date String
  readonly lastModifiedDate: string; // JSON Date String
};

export type BasicManualOrder = Omit<BasicOrder, 'orderDate'> & {
  readonly creationDate: string; // JSON Date String
}

export type AbandonedCheckout = {
  readonly id: string | null;
  readonly date: string; // JSON Date String
};

export type Transaction = {
  readonly id: string | null;
  readonly date: string; // JSON Date String
};

export type OrderPayment = {
  status: PaymentStatus;
};

export type OrderFulfillment = {
  status: FulfillmentStatus;
};

export type DigitalOrderFulfillment = OrderFulfillment;

export type PhysicalOrderFulfillment = OrderFulfillment;

export type OrderDelivery = {
  status: DeliveryStatus;
};

export type DigitalOrderDelivery = OrderDelivery;

export type PhysicalOrderDelivery = OrderDelivery;

export enum PaymentStatus {
  Paid,
  'On Delivery',
  Cancelled,
  Disputed,
  'Partially Refunded',
  Refunded,
  'Awaiting Payment',
}
export enum FulfillmentStatus {
  'Awaiting Fulfillment',
  Digital,
  Fulfilled,
}

export enum DeliveryStatus {
   Delivered,
  'Awaiting Shipment',
  'Awaiting Delivery', // Used for Digital products in draft
   Shipped,
  'In Transit',
  'Awaiting Pickup',
   Returned,
}
