import { VideoGuide } from '@/types';

const videoGuideData: { name: string; data: VideoGuide }[] = [
  {
    name: 'orders',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'transactions',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'abandoned',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'products',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'collections',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'coupons',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'storeFront',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'funnelFront',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'customers',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'subscribers',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'splittest',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'shipping',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'tax',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'customerupdates',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'customerupdateshistory',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'restricted',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'archive',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
];

export default videoGuideData;
