import { Currency } from '@/types';

const currencies: Currency[] = [
  {
    id: 1,
    name: 'US Dollar - USD ($)',
  },
  {
    id: 2,
    name: 'Afghan Afghani - AFN (؋)',
  },
  {
    id: 3,
    name: 'Algerian Dinar - DZD (دج)',
  },
  {
    id: 4,
    name: 'Argentine Peso - ARS ($)',
  },
  {
    id: 5,
    name: 'Armenian Dram - AMD (AMD)',
  },
  {
    id: 6,
    name: 'Aruban Guilder - AWG (ƒ)',
  },
  {
    id: 7,
    name: 'Australian Dollar - AUD (AU$)',
  },
  {
    id: 8,
    name: 'Azerbaijan Manat - AZN (ман)',
  },
  {
    id: 9,
    name: 'Bahamian Dollar - BSD ($)',
  },
  {
    id: 10,
    name: 'Bahraini Dinar - BHD (.د.ب)',
  },
  {
    id: 11,
    name: 'Baht - THB (฿)',
  },
  {
    id: 12,
    name: 'Balboa - PAB (B/.)',
  },
  {
    id: 13,
    name: 'Barbados Dollar - BBD ($)',
  },
  {
    id: 14,
    name: 'Belarusian Ruble - BYN (BYR)',
  },
  {
    id: 15,
    name: 'Belize Dollar - BZD (BZ$)',
  },
  {
    id: 16,
    name: 'Bermudian Dollar - BMD (BD$)',
  },
  {
    id: 17,
    name: 'Boliviano - BOB ($b)',
  },
  {
    id: 18,
    name: 'Bolívar fuerte venezolano - VEF (Bs)',
  },
  {
    id: 19,
    name: 'Brazilian Real - BRL (R$)',
  },
  {
    id: 20,
    name: 'Brunei Dollar - BND ($)',
  },
  {
    id: 21,
    name: 'Bulgarian Lev - BGN (лв)',
  },
  {
    id: 22,
    name: 'Burundi Franc - BIF (FBu)',
  },
  {
    id: 23,
    name: 'CFA Franc BCEAO - XOF (CFA)',
  },
  {
    id: 24,
    name: 'CFA Franc BEAC - XAF (FCFA)',
  },
  {
    id: 25,
    name: 'CFP Franc - XPF (F)',
  },
  {
    id: 26,
    name: 'Canadian Dollar - CAD (C$)',
  },
  {
    id: 27,
    name: 'Cape Verde Escudo - CVE ($)',
  },
  {
    id: 28,
    name: 'Cayman Islands Dollar - KYD ($)',
  },
  {
    id: 29,
    name: 'Chilean Peso - CLP ($)',
  },
  {
    id: 30,
    name: 'Colombian Peso - COP ($)',
  },
  {
    id: 31,
    name: 'Comoro Franc - KMF (CF)',
  },
  {
    id: 32,
    name: 'Convertible Mark - BAM (KM)',
  },
  {
    id: 33,
    name: 'Cordoba Oro - NIO (C$)',
  },
  {
    id: 34,
    name: 'Costa Rican Colon - CRC (₡)',
  },
  {
    id: 35,
    name: 'Croatian Kuna - HRK (kn)',
  },
  {
    id: 36,
    name: 'Czech Koruna - CZK (Kč)',
  },
  {
    id: 37,
    name: 'Dalasi - GMD (D)',
  },
  {
    id: 38,
    name: 'Danish Krone - DKK (kr)',
  },
  {
    id: 39,
    name: 'Djibouti Franc - DJF (Fdj)',
  },
  {
    id: 40,
    name: 'Dobra - STD (Db)',
  },
  {
    id: 41,
    name: 'Dominican Peso - DOP (RD$)',
  },
  {
    id: 42,
    name: 'Dong - VND (₫)',
  },
  {
    id: 43,
    name: 'East Carribbean Dollar - XCD ($)',
  },
  {
    id: 44,
    name: 'Egyptian Pound - EGP (£)',
  },
  {
    id: 45,
    name: 'Ethiopian Birr - ETB (Br)',
  },
  {
    id: 46,
    name: 'Euro - EUR (€)',
  },
  {
    id: 47,
    name: 'Falkland Islands Pound - FKP (£)',
  },
  {
    id: 48,
    name: 'Fiji Dollar - FJD ($)',
  },
  {
    id: 49,
    name: 'Forint - HUF (Ft)',
  },
  {
    id: 50,
    name: 'Franc Congolais - CDF (FC)',
  },
  {
    id: 51,
    name: 'Ghanaian Cedi - GHS (GH₵)',
  },
  {
    id: 52,
    name: 'Gibraltar Pound - GIP (£)',
  },
  {
    id: 53,
    name: 'Gourde - HTG (G)',
  },
  {
    id: 54,
    name: 'Guarani - PYG (Gs)',
  },
  {
    id: 55,
    name: 'Guinea Franc - GNF (FG)',
  },
  {
    id: 56,
    name: 'Guyana Dollar - GYD ($)',
  },
  {
    id: 57,
    name: 'Hong Kong Dollar - HKD (HK$)',
  },
  {
    id: 58,
    name: 'Hryvnia - UAH (₴)',
  },
  {
    id: 59,
    name: 'Iceland Krona - ISK (kr)',
  },
  {
    id: 60,
    name: 'Indian Rupee - INR (₹)',
  },
  {
    id: 61,
    name: 'Iraqi Dinar - IQD (ع.د)',
  },
  {
    id: 62,
    name: 'Israeli New Sheqel - ILS (₪)',
  },
  {
    id: 63,
    name: 'Jamaican Dollar - JMD (J$)',
  },
  {
    id: 64,
    name: 'Jordanian Dinar - JOD (JD)',
  },
  {
    id: 65,
    name: 'Kenyan Shilling - KES (Ksh)',
  },
  {
    id: 66,
    name: 'Kina - PGK (K)',
  },
  {
    id: 67,
    name: 'Kip - LAK (₭)',
  },
  {
    id: 68,
    name: 'Korean Won - KRW (₩)',
  },
  {
    id: 69,
    name: 'Kuwaiti Dinar - KWD (د.ك)',
  },
  {
    id: 70,
    name: 'Kwacha - MWK (MK)',
  },
  {
    id: 71,
    name: 'Kwanza - AOA (Kz)',
  },
  {
    id: 72,
    name: 'Kyat - MMK (K)',
  },
  {
    id: 73,
    name: 'Lari - GEL (GEL)',
  },
  {
    id: 74,
    name: 'Lebanese Pound - LBP (.ل.ل)',
  },
  {
    id: 75,
    name: 'Lek - ALL (Lek)',
  },
  {
    id: 76,
    name: 'Lempira - HNL (L)',
  },
  {
    id: 77,
    name: 'Leone - SLL (LE)',
  },
  {
    id: 78,
    name: 'Liberian Dollar - LRD ($)',
  },
  {
    id: 79,
    name: 'Libyan dinar - LYD (ل.د)',
  },
  {
    id: 80,
    name: 'Lilangeni - SZL (L)',
  },
  {
    id: 81,
    name: 'Loti - LSL (L)',
  },
  {
    id: 82,
    name: 'Macedonian Denar - MKD (ден)',
  },
  {
    id: 83,
    name: 'Malagasy Ariary - MGA (Ar)',
  },
  {
    id: 84,
    name: 'Malaysian Ringgit - MYR (RM)',
  },
  {
    id: 85,
    name: 'Mauritius Rupee - MUR (₨)',
  },
  {
    id: 86,
    name: 'Mexican Peso - MXN ($)',
  },
  {
    id: 87,
    name: 'Moldovan Leu - MDL (L)',
  },
  {
    id: 88,
    name: 'Moroccan Dirham - MAD (د.م.)',
  },
  {
    id: 89,
    name: 'Mozambican Metical - MZN (MT)',
  },
  {
    id: 90,
    name: 'Naira - NGN (₦)',
  },
  {
    id: 91,
    name: 'Nakfa - ERN (ናቕፋ)',
  },
  {
    id: 92,
    name: 'Namibian Dollar - NAD ($)',
  },
  {
    id: 93,
    name: 'Nepalese Rupee - NPR (₨)',
  },
  {
    id: 94,
    name: 'Netherlands Antillan Guilder - ANG (ƒ)',
  },
  {
    id: 95,
    name: 'New Taiwan Dollar - TWD (NT$)',
  },
  {
    id: 96,
    name: 'New Zealand Dollar - NZD ($)',
  },
  {
    id: 97,
    name: 'Ngultrum - BTN (Nu.)',
  },
  {
    id: 98,
    name: 'Norwegian Krone - NOK (kr)',
  },
  {
    id: 99,
    name: 'Nuevo Sol - PEN (S/.)',
  },
  {
    id: 100,
    name: 'Ouguiya - MRO (UM)',
  },
  {
    id: 101,
    name: "Pa'anga - TOP (T$)",
  },
  {
    id: 102,
    name: 'Pakistan Rupee - PKR (₨)',
  },
  {
    id: 103,
    name: 'Pataca - MOP (MOP$)',
  },
  {
    id: 104,
    name: 'Peso Uruguayo - UYU ($U)',
  },
  {
    id: 105,
    name: 'Philippine Peso - PHP (₱)',
  },
  {
    id: 106,
    name: 'Pound Sterling - GBP (£)',
  },
  {
    id: 107,
    name: 'Pula - BWP (P)',
  },
  {
    id: 108,
    name: 'Qatari Rial - QAR (﷼)',
  },
  {
    id: 109,
    name: 'Quetzal - GTQ (Q)',
  },
  {
    id: 110,
    name: 'Rand - ZAR (R)',
  },
  {
    id: 111,
    name: 'Rial Omani - OMR (﷼)',
  },
  {
    id: 112,
    name: 'Riel - KHR (៛)',
  },
  {
    id: 113,
    name: 'Romanian Lei - RON (lei)',
  },
  {
    id: 114,
    name: 'Rufiyaa - MVR (Rf)',
  },
  {
    id: 115,
    name: 'Rupiah - IDR (Rp)',
  },
  {
    id: 116,
    name: 'Russian Ruble - RUB (руб.)',
  },
  {
    id: 117,
    name: 'Rwanda Franc - RWF (R₣)',
  },
  {
    id: 118,
    name: 'Saint Helena Pound - SHP (£)',
  },
  {
    id: 119,
    name: 'Saudi Riyal - SAR (﷼)',
  },
  {
    id: 120,
    name: 'Serbian Dinar - RSD (РСД)',
  },
  {
    id: 121,
    name: 'Seychelles Rupee - SCR (₨)',
  },
  {
    id: 122,
    name: 'Singapore Dollar - SGD (S$)',
  },
  {
    id: 123,
    name: 'Solomon Islands Dollar - SBD ($)',
  },
  {
    id: 124,
    name: 'Som - KGS (сом)',
  },
  {
    id: 125,
    name: 'Somali Shilling - SOS (S)',
  },
  {
    id: 126,
    name: 'Somoni - TJS (TJS)',
  },
  {
    id: 127,
    name: 'Sri Lanka Rupee - LKR (₨)',
  },
  {
    id: 128,
    name: 'Surinamese Dollar - SRD ($)',
  },
  {
    id: 129,
    name: 'Swedish Krona - SEK (kr)',
  },
  {
    id: 130,
    name: 'Swiss Franc - CHF (CHF)',
  },
  {
    id: 131,
    name: 'Taka - BDT (৳)',
  },
  {
    id: 132,
    name: 'Tala - WST (WS$)',
  },
  {
    id: 133,
    name: 'Tanzanian Shilling - TZS (TSh)',
  },
  {
    id: 134,
    name: 'Tenge - KZT (₸)',
  },
  {
    id: 135,
    name: 'Trinidad and Tobago Dollar - TTD (TT$)',
  },
  {
    id: 136,
    name: 'Tugrik (Tugrug) - MNT (₮)',
  },
  {
    id: 137,
    name: 'Tunisian Dinar - TND (د.ت)',
  },
  {
    id: 138,
    name: 'Turkmenistani Manat - TMT (T)',
  },
  {
    id: 139,
    name: 'Türk lirası (TL) - TRY (₺)',
  },
  {
    id: 140,
    name: 'UAE Dirham - AED (AED)',
  },
  {
    id: 141,
    name: 'Uganda Shilling - UGX (USh)',
  },
  {
    id: 142,
    name: 'Uzbekistan Sum - UZS (сум)',
  },
  {
    id: 143,
    name: 'Vatu - VUV (VT)',
  },
  {
    id: 144,
    name: 'Yemeni Rial - YER (﷼)',
  },
  {
    id: 145,
    name: 'Yen - JPY (¥)',
  },
  {
    id: 146,
    name: 'Yuan Renminbi - CNY (¥)',
  },
  {
    id: 147,
    name: 'Zambian Kwacha - ZMW (ZK)',
  },
  {
    id: 148,
    name: 'Zloty - PLN (zł)',
  },
];

export default currencies;
