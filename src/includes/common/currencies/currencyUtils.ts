import currencies from '@/includes/common/currencies';
import { Currency } from '@/types';

function getCurrencyList(): Currency[] {
  return currencies;
}

function getCurrencyName(currency: Currency['name']) {
  return currency
    .split(' - ')[1]
    .split(' ')[0];
}

function getCurrencySymbol(currency: Currency['name']) {
  return currency
    .split(' - ')[1]
    .split(' ')[1]
    .slice(1, -1);
}

export { getCurrencyList, getCurrencyName, getCurrencySymbol };
