import isArray from 'lodash/isArray';

class ClosableMenuObserver {
  // Default value so components that only implements the default don't need to import
  // and subscribe to the observer.
  static subscribers: string[] = ['isMenuShown'];

  static subscribe(props: string[] | string) {
    if (!(isArray(props) || typeof props === 'string')) {
      throw new TypeError(`Expected an array or a string, got ${typeof props}`);
    }

    if (isArray(props)) {
      /* eslint no-restricted-syntax: 0 */
      for (const prop of props) {
        if (!this.subscribers.includes(prop)) this.subscribers.push(prop);
      }
    }

    if (!this.subscribers.includes(props as string)) {
      this.subscribers.push(props as string);
    }
  }

  static unsubscribe(props: string[] | string) {
    if (!(isArray(props) || typeof props === 'string')) {
      throw new TypeError(`Expected an array or a string, got ${typeof props}`);
    }

    if (isArray(props)) {
      for (const prop of props) {
        /* eslint no-restricted-syntax: 0 */
        const index = this.subscribers.indexOf(prop);
        if (index !== -1) this.subscribers.splice(index, 1);
      }
    }

    const index = this.subscribers.indexOf(props as string);
    if (index !== -1) this.subscribers.splice(index, 1);
  }
}

export default ClosableMenuObserver;
