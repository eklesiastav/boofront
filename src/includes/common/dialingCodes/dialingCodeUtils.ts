import dialingCodes from '@/includes/common/dialingCodes/countryDialingCodes';

function getCodeList() {
  return dialingCodes;
}

function getDialingCode(country: string) {
  return dialingCodes.find((item) => item[0] === country)?.[1];
}

function getFlagURL(country: string) {
  return dialingCodes.find((item) => item[0] === country)?.[2];
}

function formatPhoneNumber(phoneCountry: string, phoneNumber: string) {
  let phone = phoneNumber;
  const countryCode = getDialingCode(phoneCountry);
  if (phoneCountry === 'United States') {
    const areaCode = phoneNumber.slice(0, 3);
    const exchangeCode = phoneNumber.slice(3, 6);
    const assignedNumber = phoneNumber.slice(6);
    phone = ` (${areaCode}) ${exchangeCode}-${assignedNumber}`;
  }
  return `${countryCode} ${phone}`;
}

export {
  getCodeList,
  getDialingCode,
  getFlagURL,
  formatPhoneNumber,
};
