import findIndex from 'lodash/findIndex';
import findLastIndex from 'lodash/findLastIndex';
import geoLocations from './geoLocations';

/* eslint no-restricted-syntax: 0 */

/**
 * Get the first index of a given country in the geoLocations Array
 */
function getCountryFirstIndex(country: string) {
  return findIndex(
    geoLocations, (item) => item[0]?.toLocaleLowerCase() === country?.toLocaleLowerCase(),
  );
}

/**
 * Get the last index of a given country in the geoLocations Array
 */
function getCountryLastIndex(country: string) {
  return findLastIndex(
    geoLocations, (item) => item[0]?.toLocaleLowerCase() === country?.toLocaleLowerCase(),
  );
}

/**
 * Get the first index of the country with a given region name.
 */
function getRegionFirstIndex(country: string, region: string) {
  return findIndex(
    geoLocations, (item) => item[0]?.toLocaleLowerCase() === country?.toLocaleLowerCase()
        && item[1]?.toLocaleLowerCase() === region?.toLocaleLowerCase(),
  );
}

/**
 * Get the last index of the country with a given region name.
 */
function getRegionLastIndex(country: string, region: string) {
  return findLastIndex(geoLocations, (item) => item[0]
      ?.toLocaleLowerCase() === country?.toLocaleLowerCase()
      && item[1]?.toLocaleLowerCase() === region?.toLocaleLowerCase());
}

/**
 * Get a list of all countries.
 */
function getCountries() {
  const countries: string[] = [];
  for (const country of geoLocations) {
    if (!countries.includes(country[0])) {
      countries.push(country[0]);
    }
  }
  return countries;
}

/**
 * Get a list of states/regions in a particular country.
 */
function getRegions(country: string) {
  const region: string[] = [];
  const firstIndex = getCountryFirstIndex(country);
  const lastIndex = getCountryLastIndex(country);
  /* eslint no-shadow: 0 */
  for (const country of geoLocations.slice(firstIndex, lastIndex)) {
    if (!region.includes(country[1])) {
      region.push(country[1]);
    }
  }
  return region;
}

/**
 * Get a list of cities in a particular country and state/region.
 */
function getCities(country: string, region: string) {
  const cities: string[] = [];
  const firstIndex = getRegionFirstIndex(country, region);
  const lastIndex = getRegionLastIndex(country, region);

  for (const country of geoLocations.slice(firstIndex, lastIndex)) {
    if (!cities.includes(country[2])) {
      cities.push(country[2]);
    }
  }
  return cities;
}

function getRegionLabel(country: string) {
  const regionLabel = {
    county: [''],
    region: ['United Kingdom'],
    province: [''],
    provinceAndRegion: ['China'],
    provinceAndTerritory: ['Canada'],
    stateAndTerritory: ['Nigeria'],
    prefecture: ['Japan'],
  };
  if (regionLabel.county.includes(country)) return { singular: 'County', plural: 'Counties' };
  if (regionLabel.region.includes(country)) return { singular: 'Region', plural: 'Regions' };
  if (regionLabel.province.includes(country)) return { singular: 'Province', plural: 'Provinces' };
  if (regionLabel.provinceAndRegion.includes(country)) return { singular: 'Province / Region', plural: 'Provinces / Regions' };
  if (regionLabel.provinceAndTerritory.includes(country)) return { singular: 'Province / Territory', plural: 'Provinces / Territories' };
  if (regionLabel.stateAndTerritory.includes(country)) return { singular: 'State / Territory', plural: 'States / Territories' };
  if (regionLabel.prefecture.includes(country)) return { singular: 'Prefecture', plural: 'Prefectures' };
  return { singular: 'State', plural: 'States' };
}

export {
  getCountries,
  getRegions, getCities,
  getRegionLabel,
};
