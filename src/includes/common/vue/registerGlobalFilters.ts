import { VueConstructor } from 'vue';
import moment from 'moment';
import startCase from 'lodash/startCase';

const filters = [
  {
    name: 'lower',
    func(value: string) {
      if (!value) return '';
      return value.toLocaleLowerCase();
    },
  },
  {
    name: 'upper',
    func(value: string) {
      if (!value) return '';
      return value.toUpperCase();
    },
  },
  {
    name: 'capitalize',
    func(value: string) {
      if (!value) return '';
      const valueList = value.split(' ');
      const newList = [];
      for (const word of valueList) {
        newList.push(word[0].toUpperCase() + word.slice(1));
      }
      return newList.join(' ');
    },
  },
  {
    name: 'initials',
    func(value: string) {
      if (!value) return '';
      const [first, last] = value.split(' ');
      if (!first || !last) return '';
      return `${first[0].toUpperCase()}${last[0].toUpperCase()}`;
    },
  },
  {
    name: 'startCase',
    func(value: string) {
      if (!value) return '';
      return startCase(value);
    },
  },
  {
    name: 'monthAndShortYear',
    func(value: [string, string]) {
      if (!value?.length) return '';
      const [month, year] = value;
      return `${month}/${year.slice(-2)}`;
    },
  },
  {
    name: 'relativeTimeDay',
    func(value: string) {
      return moment.parseZone(value).startOf('day').fromNow();
    },
  },
  {
    name: 'relativeTimeHour',
    func(value: string) {
      return moment.parseZone(value).startOf('hour').fromNow();
    },
  },
  {
    name: 'relativeTimeMinute',
    func(value: string) {
      return moment.parseZone(value).startOf('minute').fromNow();
    },
  },
  {
    name: 'relativeTimeSecond',
    func(value: string) {
      return moment.parseZone(value).startOf('second').fromNow();
    },
  },
  {
    name: 'exactDate',
    func(value: string) {
      return moment.parseZone(value).format('MMM Do[,] YYYY');
    },
  },
  {
    name: 'exactDateTime',
    func(value: string) {
      return moment.parseZone(value).format('MMM Do[,] YYYY [at] h:mm A');
    },
  },
  {
    name: 'simpleFileSize',
    func(value: number, precision = 1) {
      const REF_BYTES = 1000;
      if (Math.abs(value) < REF_BYTES) {
        return `${value} B`;
      }

      const units = ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
      let u = -1;
      let size = value;
      const r = 10 ** precision;

      do {
        size /= REF_BYTES;
        u += 1;
      } while (Math.round(Math.abs(size) * r) / r >= REF_BYTES && u < units.length - 1);

      return `${size.toFixed(precision)} ${units[u]}`;
    },
  },
  {
    // Simplifies number display; e.g. 12345.6 is formatted as 12,345.6
    // and 1234.5% is formatted as 1,234.5%
    name: 'simpleNumberFormat',
    func(value: number | string) {
      let decimal = '';
      if (value.toString().includes('.')) {
        /* eslint no-param-reassign: 0 */
        [value, decimal] = value.toString().split('.');
      }
      const reversed = value.toString().split('').reverse().join('');
      const simplified = reversed
        .match(/.{1,3}/g)
        ?.map((item) => item.split('').reverse().join(''))
        .reverse()
        .join(',');
      return decimal ? `${simplified}.${decimal}` : simplified;
    },
  },
];

function registerGlobalFilters(Vue: VueConstructor) {
  /* eslint no-restricted-syntax: 0 */
  for (const filter of filters) {
    Vue.filter(filter.name, filter.func);
  }
}

export default registerGlobalFilters;
