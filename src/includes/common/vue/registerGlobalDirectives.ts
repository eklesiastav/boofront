// @ts-nocheck
/* eslint no-param-reassign: 0 */

import { VNode, VueConstructor } from 'vue';
import ClosableMenuObserver from '../observers/ClosableMenuObserver';

const directives = [
  {
    name: 'clickOutside',
    bind(el: Element, _binding, vnode: VNode) {
      /* eslint func-names: 0 */
      window.$menuClick = function (event: MouseEvent) {
        const { target } = event;
        if (!el.contains(target as Element) && el !== target) {
          for (const prop of ClosableMenuObserver.subscribers) {
            if (vnode.context[prop]) {
              vnode.context[prop] = false;
            }
          }
        }
      };
      document.addEventListener('click', window.$menuClick);
    },
    unbind() {
      document.removeEventListener('click', window.$menuClick);
    },
  },
];

/**
 * Directive to hide dropdown and select menus when the user clicks outside
 * the menu area.
 * Target element must include a v-if control boolean data named "showMenu" for
 * auto-hiding to work.
 * @param Vue
 */
function registerGlobalDirectives(Vue: VueConstructor) {
  /* eslint no-restricted-syntax: 0 */
  for (const directive of directives) {
    Vue.directive(directive.name, {
      bind: directive.bind,
      unbind: directive.unbind,
    });
  }
}

export default registerGlobalDirectives;
