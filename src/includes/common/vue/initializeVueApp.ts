/* eslint func-names: 0, no-param-reassign: 0 */

import { VueConstructor } from 'vue';
import { Store } from 'vuex';
import { Permission, UserFeedback } from '@/types';
import APIClient from '@/services/APIClient';
import TokenService from '@/services/authentication/TokenService';
import PermissionUtils from '@/includes/common/permissions/PermissionUtils';
import {
  PermissionDeniedError,
  SuspendedAccountError,
  UnapprovedWorkScheduleError,
  UnauthorizedAPIServiceError,
  UnauthorizedIPError,
  ResourceNotFound,
} from '@/exceptions';
import { loadLanguageAsync } from '@/i18n/setup';
import interfaceLanguages, { LanguageCode } from '@/i18n/interfaceLanguages';
// import ErrorCentralService from "@/services/common/ErrorCentralService";

/* eslint import/no-mutable-exports: 0 */
// Holds reference to the original Vue instance; needed to allow global user and mutation of the
// color scheme variable without any need of imports in components.
let vInstance: VueConstructor;

class AppInitializations {
  initVuePrototype(Vue: VueConstructor, store: Store<any>) {
    vInstance = Vue;

    Vue.prototype.$showFeedback = (feedbackDetail: UserFeedback) => {
      store.dispatch('showFeedback', feedbackDetail);
    };

    Vue.prototype.$playVideoGuide = (guideName: string) => {
      store.commit('playVideoGuide', guideName);
    };

    Vue.prototype.$checkPermission = async (permissions: Array<Permission['codeName']>) => {
      await PermissionUtils.checkPermission(permissions);
    };

    Vue.prototype.$showPermissionDeniedModal = () => {
      store.commit('showPermissionDeniedModal');
    };

    Vue.prototype.$hidePermissionDeniedModal = () => {
      store.commit('hidePermissionDeniedModal');
    };

    Vue.prototype.$resetFieldError = function (validityObjectName: string) {
      this[validityObjectName].isValid = true;
      this[validityObjectName].errorMessage = '';
    };

    Vue.prototype.$scrollToError = () => {
      const allInputs = document.querySelectorAll('input');
      const inputsWithError = Array.from(allInputs).filter(
        (node) => node.classList.contains('input-error'),
      );
      if (inputsWithError.length > 0) {
        const heightOfFirstInputWithError = inputsWithError[0].getBoundingClientRect().top
          + window.pageYOffset;
        window.scrollTo({
          left: 0,
          top: heightOfFirstInputWithError - 280,
          behavior: 'smooth',
        });
      }
    };

    Vue.prototype.$colorScheme = 'light';
  }

  /**
   * Checks if the current session is authenticated and load language pack from user profile.
   * If session is not authenticated, check if anonymous language was set for the visitor in a prior
   * visit. (Set from the 'ExternalLanguageSelector' component) and load appropriate pack.
   * Else, default to 'en'.
   * @param store
   */
  async setSessionLanguage(store: Store<any>) {
    let sessionLang;
    const isAuthenticated = !!TokenService.getToken();
    const anonymousLang = localStorage.getItem('anonymousLang') as LanguageCode | null;
    const langCodes = interfaceLanguages.map((item) => item.code);

    if (isAuthenticated) {
      sessionLang = store.state.accountInformation.accountInfo.accountLanguage;
    } else if (anonymousLang && langCodes.includes(anonymousLang)) {
      sessionLang = anonymousLang;
    } else sessionLang = 'en';

    await loadLanguageAsync(sessionLang);
  }

  setVueErrorHandler(Vue: VueConstructor) {
    Vue.config.errorHandler = (error: Error, vm: Vue) => {
      if (error instanceof PermissionDeniedError) {
        /* eslint @typescript-eslint/ban-ts-ignore:0 */
        // @ts-ignore
        vm.$showPermissionDeniedModal();
      } else if (error instanceof ResourceNotFound) {
        vm.$router.push({ name: 'ResourceNotFound' });
      } else if (error instanceof UnauthorizedAPIServiceError) {
        vm.$router.push({ name: 'ResourceNotFound' });
      } else if (error instanceof UnauthorizedIPError) {
        vm.$router.push({ name: 'UnauthorizedIP' });
      } else if (error instanceof UnapprovedWorkScheduleError) {
        vm.$router.push({ name: 'UnapprovedWorkSchedule' });
      } else if (error instanceof SuspendedAccountError) {
        vm.$router.push({ name: 'SuspendedAccount' });
      } else throw error; /* ErrorCentralService.log(error, LogLevel.Fatal); */
    };
  }

  /**
   * Set base URL of the backend API, and http authentication headers if user is already
   * authenticated.
   */
  initAPIService() {
    APIClient.init(process.env.VUE_APP_URL);
    if (TokenService.getToken()) {
      APIClient.setTokenHeader();
    }
  }
}

export default function initializeVueApp(Vue: VueConstructor, store: Store<any>) {
  const initializer = new AppInitializations();
  initializer.initVuePrototype(Vue, store);
  initializer.setVueErrorHandler(Vue);
  initializer.setSessionLanguage(store);
  initializer.initAPIService();
}

export { vInstance };
