import flatMap from 'lodash/flatMap';
import store from '@/store';
import { PermissionDeniedError } from '@/exceptions';
import { AccountInformation, Role, Permission } from '@/types';

class PermissionUtils {
  /**
   * Checks if the currently authenticated user belongs to a role that has all the permissions
   * in the permissions array.
   * It raises a permission denied error that can be handled by the caller or by the global handler
   * which defaults to showing a permission denied modal window.
   * @param {string[]} permissions
   */
  checkPermission(permissions: Array<Permission['codeName']>) {
    /* eslint prefer-destructuring: 0 */
    const accountInformation: AccountInformation = store.state.accountInformation;
    const accountRole: Role = store.state.accountRole;
    if (!accountInformation.isOwner) {
      const userPerms: Array<Permission['codeName']> = flatMap(accountRole.apps, (app) => app.permissions);
      /* eslint no-restricted-syntax: 0 */
      for (const perm of permissions) {
        if (!userPerms.includes(perm)) {
          throw new PermissionDeniedError('You do not have permission to perform this action.');
        }
      }
    }
  }
}

const permissionUtils = new PermissionUtils();
export default permissionUtils;
