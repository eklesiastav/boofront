/**
 * List of permissions for the BoomFront app.
 * The codeName, while essentially redundant ensures we can convert holistically from display format
 * and the snake case format required buy the backend. Also, the display name can be changed without
 * affecting backend implementation.
 */
const boomFrontPermissions = [
  {
    category: 'Products',
    permissions: [
      {
        name: 'Can information contents on “Products” page',
        codeName: 'can_view_contents_on_products_page',
        description: 'When disabled, the “Products” page will be hidden from the user.',
      },
      {
        name: 'Can create product',
        codeName: 'can_create_product',
        description: '',
      },
      {
        name: 'Can edit product',
        codeName: 'can_edit_product',
        description: '',
      },
      {
        name: 'Can import product(s)',
        codeName: 'can_import_products',
        description: '',
      },
      {
        name: 'Can export product(s)',
        codeName: 'can_export_products',
        description: '',
      },
      {
        name: 'Can archive product(s)',
        codeName: 'can_edit_products',
        description: '',
      },
      {
        name: 'Can delete product(s)',
        codeName: 'can_delete_products',
        description: '',
      },
    ],
  },
  {
    category: 'Fronts',
    permissions: [
      {
        name: 'Can create site, store & funnel',
        codeName: 'can_create_site_store_funnel',
        description: '',
      },
      {
        name: 'Can manage pages',
        codeName: 'can_manage_pages',
        description: '',
      },
      {
        name: 'Can edit automation',
        codeName: 'can_edit_automation',
        description: '',
      },
      {
        name: 'Can manage split tests',
        codeName: 'can_manage_split_tests',
        description: '',
      },
      {
        name: 'Can export funnel(s)',
        codeName: 'can_export_funnels',
        description: '',
      },
      {
        name: 'Can import funnel(s)',
        codeName: 'can_import_funnels',
        description: '',
      },
      {
        name: 'Can archive funnel(s)',
        codeName: 'can_archive_funnels',
        description: '',
      },
      {
        name: 'Can delete funnel(s)',
        codeName: 'can_delete_funnels',
        description: '',
      },
    ],
  },
  {
    category: 'Orders',
    permissions: [
      {
        name: 'Can information contents on “Orders” page',
        codeName: 'can_view_contents_on_orders_page',
        description: 'When disabled, the “Orders” page will be hidden from the user.',
      },
      {
        name: 'Can create order',
        codeName: 'can_create_order',
        description: '',
      },
      {
        name: 'Can edit order',
        codeName: 'can_edit_order',
        description: '',
      },
      {
        name: 'Can archive order',
        codeName: 'can_archive_order',
        description: '',
      },
      {
        name: 'Can cancel order',
        codeName: 'can_cancel_order',
        description: '',
      },
      {
        name: 'Can refund order',
        codeName: 'can_refund_order',
        description: '',
      },
    ],
  },
  {
    category: 'Customers',
    permissions: [
      {
        name: 'Can view “Customers” or "Subscribers" page',
        codeName: 'can_view_customers_or_subscribers_page',
        description: 'When disabled, the “Customers” and "Subscribers" page will be hidden from the user.',
      },
      {
        name: 'Can create customer or subscriber',
        codeName: 'can_create_customer_subscriber',
        description: '',
      },
      {
        name: 'Can view customer or subscriber detail',
        codeName: 'can_view_customer_subscriber_detail',
        description: '',
      },
      {
        name: 'Can edit customer or subscriber',
        codeName: 'can_edit_customer_subscriber',
        description: '',
      },
      {
        name: 'Can import customer(s) or subscriber(s)',
        codeName: 'can_import_customers_subscribers',
        description: '',
      },
      {
        name: 'Can export customer(s) or subscriber(s)',
        codeName: 'can_export_customers_subscribers',
        description: '',
      },
      {
        name: 'Can delete customer(s) or subscriber(s)',
        codeName: 'can_delete_customers_subscribers',
        description: '',
      },
    ],
  },
  {
    category: 'Analytics',
    permissions: [
      {
        name: 'Can information contents on “Analytics” page',
        codeName: 'can_view_contents_on_analytics_page',
        description: 'When disabled, the “Analytics” page will be hidden from the user.',
      },
      {
        name: 'Edit seller settings',
        codeName: 'edit_seller_settings',
        description: '',
      },
    ],
  },
  {
    category: 'Settings',
    permissions: [
      {
        name: 'Can edit BoomFront settings',
        codeName: 'can_edit_boomfront_settings',
        description: '',
      },
    ],
  },
  {
    category: 'Notifications',
    permissions: [
      {
        name: 'Can receive BoomFront notifications',
        codeName: 'can_receive_boomfront_notifications',
        description: '',
      },
    ],
  },
];

export default boomFrontPermissions;
