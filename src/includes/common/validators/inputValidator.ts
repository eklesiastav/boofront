import { ValueError } from '@/exceptions';
import {
  validateDomainNameRegex,
  validateEmailRegex, validateGenericNameRegex,
  validateNameRegex,
  validatePassword, validateURLRegex,
} from '@/includes/common/validators/regexValidators';
import { InputValidityObject } from '@/types';

type Data = { readonly name: string; readonly value: string | number};
type ValidatorObject = {
  readonly validator: Validator;
  readonly constraint?: number;
  readonly message?: string;
};

export enum Validator {
  NoEmpty,
  Number,
  MinLength,
  MaxLength,
  ExactLength,
  MinValue,
  MaxValue,
  ExactValue,
  Name,
  GenericName,
  Email,
  Password,
  DomainName,
  WebsiteURL,
}

function noEmpty(data: any) {
  if (!data) return false;
  return (data.toString()).length > 0;
}

function number(data: any) {
  if (!data) return false;
  /* eslint no-restricted-globals: 0 */
  if (typeof data === 'string') return !isNaN(parseFloat(data));
  return typeof data === 'number';
}

function minLength(data: any, constraint: number) {
  return (data as string).length >= constraint;
}

function maxLength(data: any, constraint: number) {
  return (data as string).length <= constraint;
}

function exactLength(data: any, constraint: number) {
  return (data as string).length === constraint;
}

function minValue(data: any, constraint: number) {
  if (typeof data === 'string') return parseFloat(data) >= constraint;
  return data >= constraint;
}

function maxValue(data: any, constraint: number) {
  if (typeof data === 'string') return parseFloat(data) <= constraint;
  return data <= constraint;
}

function exactValue(data: any, constraint: number) {
  if (typeof data === 'string') return parseFloat(data) === constraint;
  return data === constraint;
}

function name(data: any) {
  if (!data) return false;
  return validateNameRegex(data);
}

function genericName(data: any) {
  if (!data) return false;
  return validateGenericNameRegex(data);
}

function email(data: any) {
  if (!data) return false;
  return validateEmailRegex(data);
}

function password(data: any) {
  if (!data) return false;
  return validatePassword(data);
}

function domainName(data: any) {
  if (!data) return false;
  return validateDomainNameRegex(data);
}

function websiteURL(data: any) {
  if (!data) return false;
  return validateURLRegex(data);
}

function getValidatorFunction(validator: Validator) {
  switch (validator) {
    case Validator.NoEmpty:
      return noEmpty;
    case Validator.Number:
      return number;
    case Validator.MinLength:
      return minLength;
    case Validator.MaxLength:
      return maxLength;
    case Validator.ExactLength:
      return exactLength;
    case Validator.MinValue:
      return minValue;
    case Validator.MaxValue:
      return maxValue;
    case Validator.ExactValue:
      return exactValue;
    case Validator.Name:
      return name;
    case Validator.GenericName:
      return genericName;
    case Validator.Email:
      return email;
    case Validator.Password:
      return password;
    case Validator.DomainName:
      return domainName;
    case Validator.WebsiteURL:
      return websiteURL;
    default:
      throw new ValueError(`${Validator[validator]} is not supported.`);
  }
}

function getGenericMessage(data: Data, validatorObject: ValidatorObject) {
  switch (validatorObject.validator) {
    case Validator.NoEmpty:
      return `${data.name} is required.`;
    case Validator.Number:
      return `${data.name} must be a number.`;
    case Validator.MinLength:
      return `${data.name} must be at least ${validatorObject.constraint} characters.`;
    case Validator.MaxLength:
      return `${data.name} must be at most ${validatorObject.constraint} characters.`;
    case Validator.ExactLength:
      return `${data.name} must be ${validatorObject.constraint} characters.`;
    case Validator.MinValue:
      return `${data.name} must be greater than ${validatorObject.constraint}`;
    case Validator.MaxValue:
      return `${data.name} must be less than ${validatorObject.constraint}`;
    case Validator.ExactValue:
      return `${data.name} must equal ${validatorObject.constraint}`;
    case Validator.Name:
    case Validator.GenericName:
      return `Are you sure you entered the ${data.name.toLowerCase()} correctly?`;
    case Validator.Email:
      return `Are you sure you entered the ${data.name.toLowerCase()} correctly?`;
    case Validator.Password:
      return 'Please use a strong password.';
    case Validator.DomainName:
      return `Are you sure you entered the ${data.name.toLowerCase()} correctly?`;
    case Validator.WebsiteURL:
      return `Are you sure you entered the ${data.name.toLowerCase()} correctly?`;
    default:
      throw new ValueError(`${Validator[validatorObject.validator]} is not supported.`);
  }
}

export default function InputValidator(
  data: Data,
  validityObject: InputValidityObject,
  validatorObjects: ValidatorObject[],
): void {
  /* eslint no-param-reassign: 0 */
  validityObject.isValid = true;
  validityObject.errorMessage = '';
  /* eslint no-restricted-syntax: 0 */
  for (const validatorObject of validatorObjects) {
    const validatorFunction = getValidatorFunction(validatorObject.validator);
    if (!validatorFunction(data.value, validatorObject.constraint as number)) {
      validityObject.isValid = false;
      validityObject.errorMessage = validatorObject.message
        ?? getGenericMessage(data, validatorObject);
      break;
    }
  }
}
