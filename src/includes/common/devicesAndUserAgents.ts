const device = () => {
  const platform = navigator.platform.toLocaleLowerCase();

  if (platform.indexOf('mac') !== -1) return 'Mac';
  if (platform.indexOf('win') !== -1) return 'Windows';
  if (platform.indexOf('linux') !== -1) return 'Linux';
  if (platform.indexOf('iphone') !== -1) return 'iPhone';
  if (platform.indexOf('ipad') !== -1) return 'iPad';
  // Cannot properly detect android devices.
  return 'device';
};

const userAgent = () => {
  const inBrowser = typeof window !== 'undefined';
  const UA = inBrowser && window.navigator.userAgent.toLocaleLowerCase();
  const isIE = UA && /msie|trident/.test(UA);
  const isIE9 = UA && UA.indexOf('msie 9.0') > 0;
  const isEdge = UA && UA.indexOf('edge/') > 0;
  const isAndroid = (UA && UA.indexOf('android') > 0);
  const isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA));
  const isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
  const isPhantomJS = UA && /phantomjs/.test(UA);
  const isFF = UA && UA.match(/firefox\/(\d+)/);

  if (isIE) return 'IE';
  if (isIE9) return 'IE9';
  if (isEdge) return 'Edge';
  if (isAndroid) return 'Android';
  if (isIOS) return 'iOS';
  if (isChrome) return 'Chrome';
  if (isPhantomJS) return 'Phantom';
  if (isFF) return 'Firefox';
  return 'unknown';
};

export { device, userAgent };
