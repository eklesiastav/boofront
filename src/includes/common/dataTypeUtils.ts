export default function mapNumericEnumToArray(data: any): string[] {
  const map = [] as string[];
  /* eslint no-restricted-syntax: 0 */
  for (const key of Object.keys(data)) {
    if (key.match(/[0-9]/)) map.push(data[key]);
  }
  return map;
}
