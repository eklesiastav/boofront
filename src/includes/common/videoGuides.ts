import { VideoGuide } from '@/types';

const videoGuides: { name: string; data: VideoGuide }[] = [
  {
    name: 'orders',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'subscriptions',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'events',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
  {
    name: 'fastSign',
    data: {
      videoURL: 'https://www.youtube.com/embed/T75IKSXVXlc',
      guideTitle: 'This is the guide title. Override me',
      guideDescription: 'Tell the users what this guide is all about. Keep it short.',
    },
  },
];

export default videoGuides;
