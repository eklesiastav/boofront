/**
 * A list of of current BigCommand apps.
 */
import { BigCommandApp } from '@/types';

const bigCommandApps: Array<BigCommandApp> = [
  {
    id: 1,
    name: 'Home',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'home.svg',
    alt: 'Home app logo',
    link: 'https://home.bigcommand.com/',
    isPaid: false,
  },
  {
    id: 2,
    name: 'Account',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'account.svg',
    alt: 'Account app logo',
    link: 'https://account.bigcommand.com/',
    isPaid: false,
  },
  {
    id: 3,
    name: 'BoomFront',
    description: 'Build shopping cart and automated sales funnels. ',
    logo: 'boomfront.svg',
    alt: 'BoomFront app logo',
    link: 'https://boomfront.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 4,
    name: 'BoomAffiliate',
    description: 'Build shopping cart and automated sales funnels. ',
    logo: 'boomaffiliate.svg',
    alt: 'BoomAffiliate app logo',
    link: 'https://boomaffiliate.bigcommand.com/',
    isPaid: false,
  },
  {
    id: 5,
    name: 'InboxBird',
    description:
      'Powerful marketing automation platform that deliver unmatched result.',
    logo: 'inboxbird.svg',
    alt: 'InboxBird app logo',
    link: 'https://inboxbird.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 6,
    name: 'Adilo',
    description:
      'Powerful marketing automation platform that deliver unmatched result.',
    logo: 'adilo.svg',
    alt: 'Home app logo',
    link: 'https://adilo.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 7,
    name: 'Kadini',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'kadini.svg',
    alt: 'Kadini app logo',
    link: 'https://kadini.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 8,
    name: 'BigDrive',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'bigdrive.svg',
    alt: 'BigDrive app logo',
    link: 'https://bigdrive.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 9,
    name: 'Contacts',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'contacts.svg',
    alt: 'Contacts app logo',
    link: 'https://contacts.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 10,
    name: 'AdSuite',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'adsuite.svg',
    alt: 'AdSuite app logo',
    link: 'https://adsuite.bigcommand.com/',
    isPaid: true,
  },
  {
    id: 11,
    name: 'Portal',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'portal.svg',
    alt: 'Portal app logo',
    route: { name: 'Home' },
    isPaid: true,
  },
  // {
  //   id: 12,
  //   name: 'Promotely',
  //   description:
  //     'Enterprise video hosting service with automated marketing features.',
  //   logo: 'promotely.svg',
  //   alt: 'Promotely app logo',
  //   link: 'https://promotely.bigcommand.com/',
  //   isPaid: true,
  // },
  {
    id: 13,
    name: 'Help Center',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'helpcenter.svg',
    alt: 'Help Center app logo',
    link: 'https://help.bigcommand.com/',
    isPaid: false,
  },
  {
    id: 14,
    name: 'BigPartner',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'bigpartner.svg',
    alt: 'BigPartner app logo',
    link: 'https://partner.bigcommand.com/',
    isPaid: false,
  },
  {
    id: 15,
    name: 'Corina',
    description:
      'Enterprise video hosting service with automated marketing features.',
    logo: 'corina.svg',
    alt: 'Corina app logo',
    link: 'https://corina.bigcommand.com/',
    isPaid: true,
  },
];

export default bigCommandApps;
